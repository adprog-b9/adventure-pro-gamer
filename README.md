# Adventure of Pro-gamer

[![pipeline status](https://gitlab.com/adprog-b9/adventure-pro-gamer/badges/master/pipeline.svg)](https://gitlab.com/adprog-b9/adventure-pro-gamer/-/commits/master) [![coverage report](https://gitlab.com/adprog-b9/adventure-pro-gamer/badges/master/coverage.svg)](https://gitlab.com/adprog-b9/adventure-pro-gamer/-/commits/master)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=adprog-b9_adventure-pro-gamer&metric=alert_status)](https://sonarcloud.io/dashboard?id=adprog-b9_adventure-pro-gamer)

![main.PNG](image/main.PNG)

## Team

1. Ariasena Cahya Ramadhani (1906292925)
2. Vanessa Emily Agape (1906350793)
3. Nadya Aprillia (1906398566)
4. Naufal Sani (1906398723)
5. Muhammad Rizki Fauzan (1906400375)

## Ide Aplikasi 

Merupakan sebuah web based rpg game yang dimana User bermain sebagai Adventurer dan dapat memilih role Paladin, Berserker, atau Slayer untuk memulai petualangan. Kemudian user dapat merasakan berbagai event random mendapatkan reward sesuai action yang diambil. User juga bisa berbelanja di shop selama adventure dimana item tersebut akan dapat meningkatkan status dari User dan memudahkan petualangan. Suatu adventure diakhiri dengan final event melawan Boss yang cukup kuat. 

Aplikasi ini dibangun menggunakan konsep-konsep yang ada dipelajari di kelas, seperti :
* Design Pattern (Abstract Factory, Adapter, Strategy dan Template)
* Clean code, SonarCloud checkstyle dan Unit Testing
* Penerapan REST API
* Asynchronous Programming (Menggunakan AJAX dan REST API)
* Profiling (menggunakan Prometheus dan Grafana)
* Gitlab CI/CD ke Heroku


## Deployment

Main game
https://adventureprogamer.herokuapp.com/

## Game Flow

![view6.png](image/view6.png)



