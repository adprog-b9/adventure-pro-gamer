async function fetchHtmlAsText(url) {
    return await (await fetch(url)).text();
}

async function loadDifficulty() {
    const contentDiv = document.getElementById("content");
    contentDiv.innerHTML = await fetchHtmlAsText("/difficulty/");
}

$('#buttonClick').click(function(event){
    event.preventDefault();
    console.log('create in proccess');
    var heroName = document.getElementById("heroName").value;
    var role = document.getElementById("role").value;
    var urlPost = '/characterAPI/create/produce/' + heroName + '/' + role;
    $.ajax({
        method: 'POST',
        url: urlPost,
        success: function(data){
            console.log(data);
            loadDifficulty();
        }
    })
});

