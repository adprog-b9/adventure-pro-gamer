$('.buy').click(function (event) {
    event.preventDefault();
    console.log("submitted");
    console.log($(this).val());
    var itemType = $(this).val();
    var urlPost = '/shopAPI/buy/' + itemType;
    $.ajax({
        method: 'POST',
        url: urlPost,
        success: function (data) {
            console.log(data);
            var log = data.log;

            var html = "";
            html += '<li>' + log + '</li>';
            $('#log-container').append(html);

            var db = $('.db-counter').text() - data.intPrice;
            if (db >= 0) {
                $('.db-counter').text(db);
                console.log(db);
            }
        }
    })
});
