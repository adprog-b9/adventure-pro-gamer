package bsembilan.adventureprogamer.core.difficulty;

/*
 * DaBaby Difficulty Strategy class
 *
 * a class for handling chosen difficulty "DaBaby" (easy) towards current player
 * assuming reward, progress, and enemy stats (atk,def,hp) is set to normal mode
 * note : percentage means 100% = 1 , 50% = 0.5 , etc.
 *
 */

public class DaBabyStrategy implements DifficultyStrategy {
    // get method for descriptions
    public String getDescriptions(){
        return "LES GO";
    }

    // get method for reward percentage
    // reward such as DoggoBits...
    public double getRewardPercentage(){
        // return percentage (reward)
        return 1.5;
    }

    // get method for actions percentage
    public double getActionPercentage(){
        // return percentage (progress)
        return 0.5;
    }

    // get method for battle percentage
    public int getBattleInterval(){
        // return percentage (battle)
        return 4;
    }

    @Override
    public String getType(){
        return "DaBaby";
    }

}
