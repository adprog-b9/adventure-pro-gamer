package bsembilan.adventureprogamer.core.difficulty;

/*
 * Normal Difficulty Strategy class
 *
 * a class for handling chosen difficulty "Normal" towards current player
 * assuming reward, progress, and enemy stats (atk,def,hp) is set to normal mode
 * note : percentage means 100% = 1 , 50% = 0.5 , etc.
 *
 */

public class NormalStrategy implements DifficultyStrategy {
    // get method for descriptions
    public String getDescriptions(){
        return "The Middle Ground. Not too hard, but still challenging";
    }

    // get method for reward percentage
    // reward such as DoggoBits...
    public double getRewardPercentage(){
        // return percentage (reward)
        return 1;
    }

    // get method for progress / goal points percentage
    public double getActionPercentage(){
        // return percentage (progress)
        return 1;
    }

    // get method for battle percentage
    public int getBattleInterval(){
        // return percentage (battle)
        return 3;
    }

    @Override
    public String getType(){
        return "Normal";
    }

}
