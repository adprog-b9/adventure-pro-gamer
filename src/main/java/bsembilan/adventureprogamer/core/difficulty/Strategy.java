package bsembilan.adventureprogamer.core.difficulty;

/*
* Strategy interface
* has getType returning what type it is
* */

public interface Strategy {
    String getType();
}
