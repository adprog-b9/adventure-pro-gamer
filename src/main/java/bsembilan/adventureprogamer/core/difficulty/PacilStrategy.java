package bsembilan.adventureprogamer.core.difficulty;

/*
 * Pacil Difficulty Strategy class
 *
 * a class for handling chosen difficulty "Pacil" (hard) towards current player
 * assuming reward, progress, and enemy stats (atk,def,hp) is set to normal mode
 * note : percentage means 100% = 1 , 50% = 0.5 , etc.
 *
 */

public class PacilStrategy implements DifficultyStrategy {

    // get method for descriptions
    public String getDescriptions(){
        return "Feel the true experience of Pacil Warrior in this mode";
    }

    // get method for reward percentage
    // reward such as DoggoBits...
    public double getRewardPercentage(){
        // return percentage (reward)
        return 0.5;
    }

    // get method for progress / goal points percentage
    public double getActionPercentage(){
        // return percentage (progress)
        return 1.5;
    }

    // get method for battle percentage
    public int getBattleInterval(){
        // return percentage (battle)
        return 2;
    }

    @Override
    public String getType(){
        return "Pacil";
    }

}
