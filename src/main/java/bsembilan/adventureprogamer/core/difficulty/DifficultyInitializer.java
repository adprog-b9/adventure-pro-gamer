package bsembilan.adventureprogamer.core.difficulty;

/*
* Difficulty Feature Initializer
* for initialize purposes before running the program
*
* */

import bsembilan.adventureprogamer.repository.difficultyrepository.DifficultyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class DifficultyInitializer {
    @Autowired
    private DifficultyRepository difficultyRepository;

    @PostConstruct
    public void init() {
        this.difficultyRepository.save(new DaBabyStrategy());
        this.difficultyRepository.save(new NormalStrategy());
        this.difficultyRepository.save(new PacilStrategy());
    }
}
