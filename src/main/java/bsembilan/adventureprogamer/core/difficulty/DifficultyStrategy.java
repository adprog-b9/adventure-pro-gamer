package bsembilan.adventureprogamer.core.difficulty;

/*
 * Difficulty Strategy interface
 *
 * an interface for all kinds of difficulty strategies
 *
 */

public interface DifficultyStrategy extends Strategy {
    String getDescriptions();
    double getRewardPercentage();
    double getActionPercentage();
    int getBattleInterval();

}
