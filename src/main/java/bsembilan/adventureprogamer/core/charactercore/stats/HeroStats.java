package bsembilan.adventureprogamer.core.charactercore.stats;

public class HeroStats implements Stats {
    private int maxHealth;
    private int maxAttack;
    private int maxDefense;
    private int experience;
    private Integer DB;

    /**
     * constructor for HeroStats. Provides a default 0 for experience and 100 for anything else.
     */
    public HeroStats(String role) {
        this.maxHealth = 100;
        this.experience = 20;
        this.DB = 0;
        if (role.equals("Paladin")){
            this.maxAttack = 100;
            this.maxDefense = 150;
        }
        else if (role.equals("Slayer")){
            this.maxAttack = 150;
            this.maxDefense = 100;
        }
        else if (role.equals("Berserker")){
            this.maxAttack = 125;
            this.maxDefense = 125;
        }
    }

    public int getMaxHealth() {
        return this.maxHealth;
    }

    public int getMaxAttack() {
        return this.maxAttack;
    }

    public int getMaxDefense() {
        return this.maxDefense;
    }

    public int getExperience() {
        return this.experience;
    }

    public Integer getDB() {
        return this.DB;
    }

    public void setMaxHealth(int amount) {
        this.maxHealth = amount;
    }

    public void setMaxAttack(int amount) {
        this.maxAttack = amount;
    }

    public void setMaxDefense(int amount) {
        this.maxDefense = amount;
    }

    public void addDB(Integer db) {
        this.DB += db;

        if(this.DB < 0) {
            this.DB = 0;
        }

    }

    public void addHealth(int amount) {
        this.maxHealth += amount;
    }

    public void addExperience(int amount) {
        this.experience += amount;

        if(this.experience < 0) {
            this.experience = 0;
        }
    }
}
