package bsembilan.adventureprogamer.core.charactercore.stats;

public interface Stats {
    public int getMaxHealth();

    public void setMaxHealth(int amount);

    public Integer getDB();

    public void addDB(Integer db);

    public int getMaxAttack();

    public void setMaxAttack(int amount);

    public int getMaxDefense();

    public void setMaxDefense(int amount);

    public void addHealth(int amount);

    public void addExperience(int amount);

    public int getExperience();

}
