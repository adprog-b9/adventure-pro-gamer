package bsembilan.adventureprogamer.core.charactercore.gamechar;
import bsembilan.adventureprogamer.core.charactercore.stats.*;


public class Hero implements GameChar {
    private Stats charStats;
    private String name;
    private String role;
    private String difficulty;

    private static Hero instance;

    private Hero() {
        this.charStats = new HeroStats("Paladin");
        this.name = null;
        this.role = null;
        this.difficulty = null;
    }

    /**
     * Singleton method to return the instance of Hero or make a new one.
     * @return returns the instance of hero.
     */
    public static Hero getInstance() {
        if (instance == null) {
            instance = new Hero();
        }

        return instance;
    }

    /**
     * Function to decorate the singleton instance with needed objects, previously null.
     * @param name name of the hero.
     * @param role role of the hero.
     */
    public void createChar(String name, String role, String difficulty) {
        this.name = name;
        this.role = role;
        this.difficulty = difficulty;
        this.charStats = new HeroStats(this.role);
    }

    public void setCharStats(Stats charStats) {
        this.charStats = charStats;
    }

    public void resetCharStats() {
        this.charStats = new HeroStats(this.role);
    }

    public Stats getCharStats() {
        return this.charStats;
    }

    public String getName() {
        return this.name;
    }

    public String getRole() {
        return this.role;
    }

    public String getDifficulty() {
        return this.difficulty;
    }

    public void setDifficulty(String diff) {
        this.difficulty = diff;
    }

    public static void deleteInstance() {
        Hero.instance = null;
    }
}
