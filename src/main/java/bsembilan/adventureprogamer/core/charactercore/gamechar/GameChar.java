package bsembilan.adventureprogamer.core.charactercore.gamechar;

import bsembilan.adventureprogamer.core.charactercore.stats.*;

public interface GameChar {
    public Stats getCharStats();

    public void setCharStats(Stats charStats);

    public void resetCharStats();

    public void setDifficulty(String difficulty);

    public String getDifficulty();
}
