package bsembilan.adventureprogamer.core.shopcore;

import bsembilan.adventureprogamer.core.charactercore.gamechar.Hero;

public class AdventTusk extends Item {
    /*
    (Can still be revised)
    Advent Tusk:
        - Can increase character ATK stat by 20
        - Cost: 100 db
        - Log: You bought an Advent Tusk
     */

    @Override
    public String getType() {
        return "Advent Tusk";
    }

    @Override
    public void changeDoggoBits(Hero hero) {
        if (hero.getCharStats().getDB() >= 100) {
            hero.getCharStats().addDB(-100);
        }
    }

    @Override
    public void changeStats(Hero hero) {
        if (hero.getCharStats().getDB() >= 100) {
            hero.getCharStats().setMaxAttack(hero.getCharStats().getMaxAttack()+20);
        }
    }

    @Override
    public void specialLog() {
        this.log = "You bought an Advent Tusk";
    }

    @Override
    public String getDescription() {
        return "Increase ATK stat";
    }

    @Override
    public String getPrice() {
        return "100 db";
    }
}
