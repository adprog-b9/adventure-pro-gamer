package bsembilan.adventureprogamer.core.shopcore;

import bsembilan.adventureprogamer.core.charactercore.gamechar.Hero;

public class GoodieEnd extends Item {
    /*
    (Can still be revised)
    Goodie End:
        - Can increase character Game Progress by 20%
        - Cost: 1000 db
        - Log: You bought a Goodie End
     */

    @Override
    public String getType() {
        return "Goodie End";
    }

    @Override
    public void changeDoggoBits(Hero hero) {
        if (hero.getCharStats().getDB() >= 1000) {
            hero.getCharStats().addDB(-1000);
        }
    }

    @Override
    public void changeStats(Hero hero) {
        if (hero.getCharStats().getDB() >= 1000) {
            hero.getCharStats().addExperience(20);
        }
    }

    @Override
    public void specialLog() {
        this.log = "You bought a Goodie End";
    }

    @Override
    public String getDescription() {
        return "Increase game progress";
    }

    @Override
    public String getPrice() {
        return "1000 db";
    }
}
