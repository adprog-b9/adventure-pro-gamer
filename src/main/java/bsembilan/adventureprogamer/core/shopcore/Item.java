package bsembilan.adventureprogamer.core.shopcore;

import bsembilan.adventureprogamer.core.charactercore.gamechar.Hero;

public abstract class Item {

    protected String log = "empty";

    /*
    An Interface to provide methods for:
        - Hearty potion
        - Advent tusk
        - Define feeling, and
        - Goodie end
    */

    public void purchaseItem(Hero hero) {
        this.log(hero);
        this.changeStats(hero);
        this.changeDoggoBits(hero);
    }

    //Maybe the parameter supposed to be the character model
    public abstract void changeDoggoBits(Hero hero);

    public abstract void changeStats(Hero hero);

    public void log(Hero hero) {
        if (hero.getCharStats().getDB() < this.getIntPrice()) {
            this.log = "Your Doggo Bits are not enough!";
        } else {
            this.specialLog();
        }
    }

    public abstract void specialLog();

    public abstract String getType();

    public abstract String getDescription();

    public abstract String getPrice();

    public String getLog() {
        return this.log;
    }

    public Integer getIntPrice() {
        if (this.getType().equals("Goodie End")) {
            return 1000;
        } else {
            return 100;
        }
    }
}
