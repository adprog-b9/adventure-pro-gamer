package bsembilan.adventureprogamer.core.shopcore;

import bsembilan.adventureprogamer.core.charactercore.gamechar.Hero;

public class HeartyPotion extends Item {
    /*
    (Can still be revised)
    Hearty Potion:
        - Can increase character max HP by 50
        - Cost: 100 db
        - Log: You bought a Hearty Potion
        - If user don't have enough money:
          Log: Your Doggo Bits are not enough!
     */

    @Override
    public String getType() {
        return "Hearty Potion";
    }

    @Override
    public void changeDoggoBits(Hero hero) {
        if (hero.getCharStats().getDB() >= 100) {
            hero.getCharStats().addDB(-100);
        }
    }

    @Override
    public void changeStats(Hero hero) {
        if (hero.getCharStats().getDB() >= 100) {
            hero.getCharStats().addHealth(50);
        }
    }

    @Override
    public void specialLog() {
        this.log = "You bought a Hearty Potion";
    }

    @Override
    public String getDescription() {
        return "Increase Max HP stat";
    }

    @Override
    public String getPrice() {
        return "100 db";
    }
}
