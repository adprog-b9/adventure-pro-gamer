package bsembilan.adventureprogamer.core.shopcore;

import bsembilan.adventureprogamer.core.charactercore.gamechar.Hero;

public class DefineFeeling extends Item {
    /*
    (Can still be revised)
    Define Feeling:
        - Can increase character DEF stat by 20
        - Cost: 100 db
        - Log: You bought a Define Feeling
     */

    @Override
    public String getType() {
        return "Define Feeling";
    }

    @Override
    public void changeDoggoBits(Hero hero) {
        if (hero.getCharStats().getDB() >= 100) {
            hero.getCharStats().addDB(-100);
        }
    }

    @Override
    public void changeStats(Hero hero) {
        if (hero.getCharStats().getDB() >= 100) {
            hero.getCharStats().setMaxDefense(hero.getCharStats().getMaxDefense()+20);
        }
    }

    @Override
    public void specialLog() {
        this.log = "You bought a Define Feeling";
    }

    @Override
    public String getDescription() {
        return "Increase DEF stat";
    }

    @Override
    public String getPrice() {
        return "100 db";
    }
}
