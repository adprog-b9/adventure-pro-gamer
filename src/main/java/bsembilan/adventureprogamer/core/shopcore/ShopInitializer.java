package bsembilan.adventureprogamer.core.shopcore;

import bsembilan.adventureprogamer.repository.shoprepository.ShopRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class ShopInitializer {

    @Autowired
    private ShopRepository shopRepository;

    @PostConstruct
    public void init() {
        this.shopRepository.addItem(new AdventTusk());
        this.shopRepository.addItem(new DefineFeeling());
        this.shopRepository.addItem(new GoodieEnd());
        this.shopRepository.addItem(new HeartyPotion());
    }
}
