package bsembilan.adventureprogamer.core.eventcore;

import bsembilan.adventureprogamer.core.battleevent.adapter.BattleEventAdapter;
import bsembilan.adventureprogamer.core.battleevent.template.BattleEvent;
import bsembilan.adventureprogamer.core.battleevent.template.Dragon;
import bsembilan.adventureprogamer.core.battleevent.template.Goblin;
import bsembilan.adventureprogamer.core.eventcore.eventmain.dangerousmain.DangerousEvent;
import bsembilan.adventureprogamer.core.eventcore.eventmain.MainEvent;
import bsembilan.adventureprogamer.core.eventcore.eventmain.neutralmain.NeutralEvent;
import bsembilan.adventureprogamer.repository.eventrepository.MainEventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import bsembilan.adventureprogamer.core.eventcore.eventmain.safemain.SafeEvent;

import javax.annotation.PostConstruct;

@Component
public class EventInitializer {

    @Autowired
    private MainEventRepository mainEventRepository;

    @PostConstruct
    public void initSafeEvent() {
        var villager = new SafeEvent("Villager yang kesusahan");
        villager.setActionMin("Just leave him", "You left him alone. What a useless human you are.");
        villager.setActionMed("Let's talk to him", "You ask what's going on, but that doesn't help at all.");
        villager.setActionMax("Let's help him", "You immediately helped him get the cart out of the mud. It pays you quite a bit.");
        villager.setSafeDescription("Suddenly you see a man trying to get his cart out of the mud. He looks distressed");

        mainEventRepository.add(villager);
    }

    @PostConstruct
    public void initDangerousEvent() {
        var trap = new DangerousEvent("Jungle Trap");
        trap.setActionMin("Just skip it", "You decided not to play with the box. It could be a trap.");
        trap.setActionMed("Open carefully", "You opened it carefully and suddenly you fell into the abyss. But since you're ready, you can still hold on to the top and save yourself");
        trap.setActionMax("Open it", "Suddenly the ground beneath you collapses and you fall into a very deep abyss.");
        trap.setDangerousDescription("Immediately you see there is a gold chest that looks very old. There may be a valuable prize in it.");

        mainEventRepository.add(trap);
    }

    @PostConstruct
    public void initNeutralEvent() {
        var scenery = new NeutralEvent("Lake Toba View");
        scenery.setActionMin("I don't care", "You don't care about the view. This adventure is not to be played with.");
        scenery.setActionMed("Take a look", "You took a deep breath and looked at the view of the lake, but your mind remained focused on the goal.");
        scenery.setActionMax("Take a short break", "This adventure makes you rarely take a break. You recover energy by sitting back and looking at the view of Lake Toba and fishermen fishing.");
        scenery.setNeutralDescription("You see a pretty big lake. As far as you know, the lake is Lake Toba. The largest lake on the continent.");

        mainEventRepository.add(scenery);
    }

    @PostConstruct
    public void munchakGoblin() {
        BattleEvent enemy = new Goblin("Munchak Goblin", 500, 100);
        MainEvent event = new BattleEventAdapter(enemy);
        event.setDescription("Watch out, you are being attacked by Munchak Goblin.");
        mainEventRepository.add(event);
    }

    @PostConstruct
    public void rastaGoblin() {
        BattleEvent enemy = new Goblin("Rasta Goblin", 400, 120);
        MainEvent event = new BattleEventAdapter(enemy);
        event.setDescription("Watch out, you are being attacked by Rasta Goblin.");
        mainEventRepository.add(event);
    }

    @PostConstruct
    public void dragon() {
        BattleEvent enemy = new Dragon("Dragon", 800, 170);
        MainEvent event = new BattleEventAdapter(enemy);
        event.setDescription("A starving dragon appears upon you! Careful!");
        mainEventRepository.add(event);
    }
}
