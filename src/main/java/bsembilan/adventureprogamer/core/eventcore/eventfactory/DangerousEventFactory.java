package bsembilan.adventureprogamer.core.eventcore.eventfactory;

import bsembilan.adventureprogamer.core.eventcore.action.Action;
import bsembilan.adventureprogamer.core.eventcore.action.DangerousAction;

public class DangerousEventFactory implements MainEventFactory {

    @Override
    public Action createAction(String description, int rewardType, String resultDescription) {
        return new DangerousAction(description, rewardType, resultDescription);
    }

    @Override
    public String createDescription(String eventDescription) {
        return eventDescription;
    }
}
