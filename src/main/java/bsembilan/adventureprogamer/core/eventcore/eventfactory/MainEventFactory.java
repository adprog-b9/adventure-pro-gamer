package bsembilan.adventureprogamer.core.eventcore.eventfactory;

import bsembilan.adventureprogamer.core.eventcore.action.Action;

public interface MainEventFactory {

    public Action createAction(String description, int rewardType, String resultDescription);
    public String createDescription(String eventDescription);


}
