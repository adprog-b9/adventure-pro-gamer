package bsembilan.adventureprogamer.core.eventcore.eventfactory;

import bsembilan.adventureprogamer.core.eventcore.action.Action;
import bsembilan.adventureprogamer.core.eventcore.action.SafeAction;

public class SafeEventFactory implements MainEventFactory {


    @Override
    public Action createAction(String description, int rewardType, String resultDescription) {
        return new SafeAction(description, rewardType, resultDescription);
    }

    @Override
    public String createDescription(String eventDescription) {
        return eventDescription;
    }
}
