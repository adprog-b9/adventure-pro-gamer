package bsembilan.adventureprogamer.core.eventcore.eventfactory;

import bsembilan.adventureprogamer.core.eventcore.action.Action;
import bsembilan.adventureprogamer.core.eventcore.action.NeutralAction;

public class NeutralEventFactory implements MainEventFactory{


    @Override
    public Action createAction(String description, int rewardType, String resultDescription) {
        return new NeutralAction(description, rewardType, resultDescription);
    }

    @Override
    public String createDescription(String eventDescription) {
        return eventDescription;
    }
}
