package bsembilan.adventureprogamer.core.eventcore.eventmain.dangerousmain;

import bsembilan.adventureprogamer.core.battleevent.template.BattleEvent;
import bsembilan.adventureprogamer.core.eventcore.eventfactory.DangerousEventFactory;
import bsembilan.adventureprogamer.core.eventcore.eventmain.MainEvent;

public class DangerousEvent extends MainEvent {

    DangerousEventFactory dangerousEventFactory;
    private static final String NOTIMPLEMENTED = "Should not be implemented";

    public DangerousEvent(String eventName) {
        super(eventName, "Event");
        this.dangerousEventFactory = new DangerousEventFactory();
    }

    @Override
    public double Attack(float timing) {
        throw new UnsupportedOperationException(NOTIMPLEMENTED);
    }

    @Override
    public double Defend(float timing) {
        throw new UnsupportedOperationException(NOTIMPLEMENTED);
    }

    @Override
    public BattleEvent getBattleEvent() {
        return null;
    }

    public void setDangerousDescription(String description) {
        setDescription(dangerousEventFactory.createDescription(description));
    }

    public void setActionMin(String description, String resultDescription) {
        setMinAction(dangerousEventFactory.createAction(description,  0 , resultDescription));
        setMinActionReward(0, 2);
    }

    public void setActionMed(String description, String resultDescription) {
        setMedAction(dangerousEventFactory.createAction(description,  1 , resultDescription));
        setMedActionReward(-100, -1);
    }

    public void setActionMax(String description, String resultDescription) {
        setMaxAction(dangerousEventFactory.createAction(description,  2 , resultDescription));
        setMaxActionReward(-300, -5);
    }

}
