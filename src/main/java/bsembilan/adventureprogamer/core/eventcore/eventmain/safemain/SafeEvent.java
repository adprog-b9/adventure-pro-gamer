package bsembilan.adventureprogamer.core.eventcore.eventmain.safemain;

import bsembilan.adventureprogamer.core.battleevent.template.BattleEvent;
import bsembilan.adventureprogamer.core.eventcore.eventfactory.SafeEventFactory;
import bsembilan.adventureprogamer.core.eventcore.eventmain.MainEvent;

public class SafeEvent extends MainEvent {

    SafeEventFactory safeEventFactory;
    private static final String NOTIMPLEMENTED = "Should not be implemented";

    public SafeEvent(String eventName) {
        super(eventName, "Event");
        this.safeEventFactory = new SafeEventFactory();
    }

    @Override
    public double Attack(float timing) {
        throw new UnsupportedOperationException(NOTIMPLEMENTED);
    }

    @Override
    public double Defend(float timing) {
        throw new UnsupportedOperationException(NOTIMPLEMENTED);
    }

    @Override
    public BattleEvent getBattleEvent() {
        return null;
    }

    public void setSafeDescription(String description) {
        setDescription(safeEventFactory.createDescription(description));
    }

    public void setActionMin(String description, String resultDescription) {
        setMinAction(safeEventFactory.createAction(description,  0 , resultDescription));
        setMinActionReward(200, 2);
    }

    public void setActionMed(String description, String resultDescription) {
        setMedAction(safeEventFactory.createAction(description,  1 , resultDescription));
        setMedActionReward(300, 3);
    }

    public void setActionMax(String description, String resultDescription) {
        setMaxAction(safeEventFactory.createAction(description,  2 , resultDescription));
        setMaxActionReward(500, 4);
    }

}