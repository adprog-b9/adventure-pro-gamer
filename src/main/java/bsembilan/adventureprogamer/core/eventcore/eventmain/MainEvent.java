package bsembilan.adventureprogamer.core.eventcore.eventmain;

import bsembilan.adventureprogamer.core.battleevent.template.BattleEvent;
import bsembilan.adventureprogamer.core.eventcore.action.Action;

public abstract class MainEvent {
    private String description;

    private String eventName;

    private String type;

    private Action minAction;

    private Action medAction;

    private Action maxAction;

    private boolean hasChoosen;

    protected MainEvent(String eventName, String type) {
        this.eventName = eventName;
        this.hasChoosen = true;
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Action getMinAction() {
        return minAction;
    }

    public void setMinAction(Action minAction) {
        this.minAction = minAction;
    }

    public Action getMedAction() {
        return medAction;
    }

    public void setMedAction(Action medAction) {
        this.medAction = medAction;
    }

    public Action getMaxAction() {
        return maxAction;
    }

    public void setMaxAction(Action maxAction) {
        this.maxAction = maxAction;
    }

    public String getEventName() {
        return eventName;
    }

    public void setHasChoosen(boolean state) {
        hasChoosen = state;
    }

    public boolean getHasChoosen() {
        return this.hasChoosen;
    }

    public String getType() {return  this.type; }

    public void setType(String type) { this.type = type; }

    // Placeholder method. Waiting for Character Creation Implementation.
    public void setMinActionReward(int doggobits, int gameprogress) {
        this.minAction.assignRewards(doggobits, gameprogress);
    }
    public void setMedActionReward(int doggobits, int gameprogress) {
        this.medAction.assignRewards(doggobits, gameprogress);
    }
    public void setMaxActionReward(int doggobits, int gameprogress) {
        this.maxAction.assignRewards(doggobits, gameprogress);
    }

    public abstract double Attack(float timing);

    public abstract double Defend(float timing);

    public abstract BattleEvent getBattleEvent();

    public abstract void setActionMin(String des, String resDes);
    public abstract void setActionMed(String des, String resDes);
    public abstract void setActionMax(String des, String resDes);

}
