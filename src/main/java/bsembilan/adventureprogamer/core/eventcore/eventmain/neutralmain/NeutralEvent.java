package bsembilan.adventureprogamer.core.eventcore.eventmain.neutralmain;

import bsembilan.adventureprogamer.core.battleevent.template.BattleEvent;
import bsembilan.adventureprogamer.core.eventcore.eventfactory.NeutralEventFactory;
import bsembilan.adventureprogamer.core.eventcore.eventmain.MainEvent;

public class NeutralEvent extends MainEvent {

    NeutralEventFactory neutralEventFactory;
    private static final String NOTIMPLEMENTED = "Should not be implemented";

    public NeutralEvent(String eventName) {
        super(eventName, "Event");
        this.neutralEventFactory = new NeutralEventFactory();
    }

    @Override
    public double Attack(float timing) {
        throw new UnsupportedOperationException(NOTIMPLEMENTED);
    }

    @Override
    public double Defend(float timing) {
        throw new UnsupportedOperationException(NOTIMPLEMENTED);
    }

    @Override
    public BattleEvent getBattleEvent() {
        return null;
    }

    public void setNeutralDescription(String description) {
        setDescription(neutralEventFactory.createDescription(description));
    }

    public void setActionMin(String description, String resultDescription) {
        setMinAction(neutralEventFactory.createAction(description,  0 , resultDescription));
        setMinActionReward(0, 2);
    }

    public void setActionMed(String description, String resultDescription) {
        setMedAction(neutralEventFactory.createAction(description,  1 , resultDescription));
        setMedActionReward(10, 1);
    }

    public void setActionMax(String description, String resultDescription) {
        setMaxAction(neutralEventFactory.createAction(description,  2 , resultDescription));
        setMaxActionReward(50, 3);
    }

}