package bsembilan.adventureprogamer.core.eventcore.action;

public class SafeAction extends Action {

    public SafeAction(String description, int rewardType, String resultDescription) {
        this.description = description;
        this.rewardType = rewardType;
        this.resultDescription = resultDescription;
        this.actionCategory = 0;
    }

    @Override
    public String getDoggoRewardString() {
        return "+" + getDoggoReward() + " DoggoBits";
    }

}
