package bsembilan.adventureprogamer.core.eventcore.action;

public class Action {
    protected String description;
    protected int rewardType;
    protected int[] reward;
    protected int actionCategory;
    protected String resultDescription;

    public int getActionCategory() {
        return actionCategory;
    }

    public String getDescription() {
        return description;
    }

    public int getRewardType() {
        return rewardType;
    }

    public String getResultDescription() {
        return resultDescription;
    }

    public int getDoggoReward() {
        return this.reward[0];
    }

    public String getDoggoRewardString() {
        return getDoggoReward() + " DoggoBits";
    }

    public int getProgressReward() {
        return this.reward[1];
    }

    public void assignRewards(int doggoBits, int progress) {
        this.reward = new int[]{doggoBits, progress};
    }

}
