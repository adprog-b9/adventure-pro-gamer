package bsembilan.adventureprogamer.core.eventcore.action;

public class NeutralAction extends Action{

    public NeutralAction(String description, int rewardType, String resultDescription) {
        this.description = description;
        this.rewardType = rewardType;
        this.resultDescription = resultDescription;
        this.actionCategory = 2;
    }

    @Override
    public String getDoggoRewardString() {
        return "+" + getDoggoReward() + " DoggoBits";
    }


}
