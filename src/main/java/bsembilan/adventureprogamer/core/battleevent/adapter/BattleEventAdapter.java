package bsembilan.adventureprogamer.core.battleevent.adapter;

import bsembilan.adventureprogamer.core.battleevent.template.BattleEvent;
import bsembilan.adventureprogamer.core.eventcore.eventmain.MainEvent;

public class BattleEventAdapter extends MainEvent {

    private BattleEvent battleEvent;
    private static final String NOTIMPLEMENTED = "Should not be implemented";

    public BattleEventAdapter(BattleEvent battleEvent) {
        super(battleEvent.getEnemyName(), "Battle");
        this.battleEvent = battleEvent;
    }

    public BattleEvent getBattleEvent() {
        return battleEvent;
    }

    public double Attack(float timing) {
        /*
            if the battle finish when player's attacking, it means that the enemy is defeated. So the player wins and
            the player should get their reward.

            Reward is not implemented yet.
         */
        return battleEvent.Attack(timing);
    }

    public double Defend(float timing) {
        /*
            if the battle finish when player's attacking, it means that the enemy is defeated. So the player wins and
            the player should get their reward.

            Reward is not implemented yet.
         */
        return battleEvent.Defend(timing);
    }

    @Override
    public void setActionMin(String des, String resDes) {
        throw new UnsupportedOperationException(NOTIMPLEMENTED);
    }

    @Override
    public void setActionMed(String des, String resDes) {
        throw new UnsupportedOperationException(NOTIMPLEMENTED);
    }

    @Override
    public void setActionMax(String des, String resDes) {
        throw new UnsupportedOperationException(NOTIMPLEMENTED);
    }
}
