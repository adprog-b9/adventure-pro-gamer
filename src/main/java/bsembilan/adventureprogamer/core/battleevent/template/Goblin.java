package bsembilan.adventureprogamer.core.battleevent.template;

public class Goblin extends BattleEvent {

    public Goblin(String name, double hp, int att) {
        super(name, hp, att, 1200, 8);
    }

}
