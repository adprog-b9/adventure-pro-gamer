package bsembilan.adventureprogamer.core.battleevent.template;

import bsembilan.adventureprogamer.core.charactercore.gamechar.Hero;

public class MainEnemy extends BattleEvent{
    public MainEnemy(String name, double hp, int att, Hero currentHero, int dbReward, int gameProgress) {
        super(name, hp, att, currentHero, dbReward, gameProgress);
    }
}
