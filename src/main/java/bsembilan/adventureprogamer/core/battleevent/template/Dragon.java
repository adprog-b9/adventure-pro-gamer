package bsembilan.adventureprogamer.core.battleevent.template;

public class Dragon extends BattleEvent {

    public Dragon(String name, double hp, int att) {
        super(name, hp, att, 2000, 12);
    }
}
