package bsembilan.adventureprogamer.core.battleevent.template;

import bsembilan.adventureprogamer.core.charactercore.gamechar.Hero;

public abstract class BattleEvent {
    private double playerHP;
    private int playerDef;
    private int playerAtt;

    protected String enemyName;
    protected double enemyHP;
    protected int enemyAtt;

    private int dbReward;
    private int gameProgress;

    protected BattleEvent(String name, double hp, int att, Hero hero, int dbReward, int gameProgress) {
        this.enemyName = name;
        this.enemyHP = hp;
        this.enemyAtt = att;
        this.playerHP = hero.getCharStats().getMaxHealth();
        this.playerDef = hero.getCharStats().getMaxDefense();
        this.playerAtt = hero.getCharStats().getMaxAttack();
        this.dbReward = dbReward;
        this.gameProgress = gameProgress;
    }

    protected BattleEvent(String name, double hp, int att, int dbReward, int gameProgress) {
        this.enemyName = name;
        this.enemyHP = hp;
        this.enemyAtt = att;
        this.dbReward = dbReward;
        this.gameProgress = gameProgress;
    }

    public double Attack(float timing) {
        enemyHP = enemyHP - playerAtt * timing;

        // If enemy hp ran out, player will win. Return true
        return (enemyHP);
    }

    public double Defend(float timing) {
        float damageTaken = enemyAtt - playerDef * timing;
        if (damageTaken < 0)
            damageTaken = 0;
        playerHP = playerHP - damageTaken;

        // If player hp ran out, player will lose. Return true
        return(playerHP);
    }

    public String getEnemyName() {
        return enemyName;
    }

    public double getEnemyHP() {
        return enemyHP;
    }

    public int getEnemyAtt() {
        return enemyAtt;
    }

    public double getPlayerHP() {
        return playerHP;
    }

    public int getPlayerDef() {
        return playerDef;
    }

    public int getPlayerAtt() {
        return playerAtt;
    }

    public int getDbReward() { return dbReward; }

    public int getGameProgress() { return gameProgress; }
}
