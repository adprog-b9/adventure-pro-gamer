package bsembilan.adventureprogamer.service.maineventservice;

import bsembilan.adventureprogamer.core.eventcore.action.Action;
import bsembilan.adventureprogamer.core.eventcore.eventmain.MainEvent;
import java.util.ArrayList;

public interface MainEventService {

    MainEvent getRandomEvent();
    MainEvent createMainEvent(String eventName, String eventType, String description);
    Action setAction(String eventName, String actionDes, String resultDes, int type);
    MainEvent getEvent(String eventName);
    String decodeEventName(String eventName);
    Action chooseAction(MainEvent currentEvent, int choosenAction);
    void setChoosen(MainEvent currentEvent, boolean state);
    void clearOngoingEvent();
    void addOngoingEvent(MainEvent mainEvent);
    ArrayList<MainEvent> getOnGoingEvent();
    void setIsTheEnd(boolean isTheEnd);
    boolean getIsTheEnd();
}
