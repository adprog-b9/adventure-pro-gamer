package bsembilan.adventureprogamer.service.maineventservice;

import bsembilan.adventureprogamer.core.eventcore.action.Action;
import bsembilan.adventureprogamer.core.eventcore.eventmain.dangerousmain.DangerousEvent;
import bsembilan.adventureprogamer.core.eventcore.eventmain.MainEvent;
import bsembilan.adventureprogamer.core.eventcore.eventmain.neutralmain.NeutralEvent;
import bsembilan.adventureprogamer.core.eventcore.eventmain.safemain.SafeEvent;
import bsembilan.adventureprogamer.repository.eventrepository.MainEventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.util.ArrayList;

@Service
public class MainEventServiceImpl implements MainEventService{

    @Autowired
    MainEventRepository mainEventRepository;

    SecureRandom randomizer = new SecureRandom();
    ArrayList<MainEvent> onGoingEvents = new ArrayList<>();
    boolean isTheEnd;

    @Override
    public MainEvent getRandomEvent() {

        if(onGoingEvents.size() == 1) {
            return onGoingEvents.get(0);
        }

        int eventNumber = mainEventRepository.getEventList().size();
        var randomizedEvent = mainEventRepository.getEventList().get(randomizer.nextInt(eventNumber));
        onGoingEvents.add(randomizedEvent);
        return randomizedEvent;
    }

    @Override
    public void clearOngoingEvent() {
        onGoingEvents.clear();
    }

    @Override
    public void addOngoingEvent(MainEvent mainEvent) {
        onGoingEvents.add(mainEvent);
    }

    @Override
    public ArrayList<MainEvent> getOnGoingEvent() {
        return this.onGoingEvents;
    }

    @Override
    public void setIsTheEnd(boolean isTheEnd) {
        this.isTheEnd = isTheEnd;
    }

    @Override
    public boolean getIsTheEnd() {
        return isTheEnd;
    }

    public MainEvent createMainEvent(String eventName, String eventType, String description) {
        MainEvent res;
        if(eventType.equals("Danger")) {
            res = new DangerousEvent(eventName);
        }else if (eventType.equals("Neutral")) {
            res = new NeutralEvent(eventName);
        } else {
            res = new SafeEvent(eventName);
        }

        res.setDescription(description);
        return mainEventRepository.add(res);

    }

    public Action setAction(String eventName, String actionDes, String resultDes, int type) {
        var mainEvent = mainEventRepository.getEvent(eventName);

        if(type == 0) {
            mainEvent.setActionMin(actionDes, resultDes);
        } else if(type == 1) {
            mainEvent.setActionMed(actionDes, resultDes);
        } else {
            mainEvent.setActionMax(actionDes, resultDes);
        }
        return null;
    }

    public MainEvent getEvent(String eventName) {
        return mainEventRepository.getEvent(eventName);
    }

    public String decodeEventName(String eventName) {
        return URLDecoder.decode(eventName, StandardCharsets.UTF_8);
    }

    public Action chooseAction(MainEvent currentEvent, int choosenAction) {

        Action choosenActionTemp;

        if(choosenAction == 0) {
            choosenActionTemp = currentEvent.getMinAction();
        } else if(choosenAction == 1) {
            choosenActionTemp = currentEvent.getMedAction();
        } else if(choosenAction == 2){
            choosenActionTemp = currentEvent.getMaxAction();
        } else {
            choosenActionTemp = null;
        }

        return choosenActionTemp;
    }

    public void setChoosen(MainEvent currentEvent, boolean state) {
        currentEvent.setHasChoosen(state);
    }

}
