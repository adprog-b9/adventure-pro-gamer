package bsembilan.adventureprogamer.service.characterservice;
import bsembilan.adventureprogamer.core.charactercore.gamechar.*;
import bsembilan.adventureprogamer.core.charactercore.stats.*;

import org.springframework.stereotype.Service;

@Service
public class HeroServiceImpl implements HeroService {
    private Hero hero;

    public HeroServiceImpl() {
        this.hero = Hero.getInstance();
    }

    public Hero getHero() {
        return this.hero;
    }

    public void setHeroCharStats(Stats charStats) {
        this.hero.setCharStats(charStats);
    }

    public Stats getHeroCharStats() {
        return this.hero.getCharStats();
    }

    public String getHeroName() {
        return this.hero.getName();
    }

    public String getHeroRole() {
        return this.hero.getRole();
    }

    public void addDB(int value) {
        this.hero.getCharStats().addDB(value);
    }

    public void addExperience(int value) {
        this.hero.getCharStats().addExperience(value);
    }

    public void createChar(String name, String role, String difficulty) {
        this.hero.createChar(name, role, difficulty);
    }

    public void resetCharInstance() {
        hero.deleteInstance();
        this.hero = Hero.getInstance();
    }

}
