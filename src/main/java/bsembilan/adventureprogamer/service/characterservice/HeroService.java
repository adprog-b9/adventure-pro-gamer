package bsembilan.adventureprogamer.service.characterservice;

import bsembilan.adventureprogamer.core.charactercore.gamechar.Hero;
import bsembilan.adventureprogamer.core.charactercore.stats.*;

public interface HeroService {
    public Hero getHero();

    public Stats getHeroCharStats();

    public void setHeroCharStats(Stats charStats);

    public String getHeroName();

    public String getHeroRole();

    public void addDB(int value);

    public void addExperience(int value);

    public void createChar(String name, String role, String difficulty);

    public void resetCharInstance();

}
