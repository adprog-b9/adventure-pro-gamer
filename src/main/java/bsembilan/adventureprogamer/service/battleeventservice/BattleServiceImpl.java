package bsembilan.adventureprogamer.service.battleeventservice;

import bsembilan.adventureprogamer.core.battleevent.template.BattleEvent;
import bsembilan.adventureprogamer.core.battleevent.template.MainEnemy;
import bsembilan.adventureprogamer.core.charactercore.gamechar.Hero;
import bsembilan.adventureprogamer.core.eventcore.eventmain.MainEvent;

import org.springframework.stereotype.Service;

@Service
public class BattleServiceImpl implements BattleService{

    int currentDBReward = 0;
    int currentExperienceReward = 0;

    @Override
    public double Attack(float timing, MainEvent currentEvent) {
        return currentEvent.Attack(timing);
    }

    @Override
    public double Defend(float timing, MainEvent currentEvent) {
        return currentEvent.Defend(timing);
    }

    @Override
    public BattleEvent createNewInstance(Hero hero, BattleEvent battleEvent) {
        BattleEvent currentBattleEvent = new MainEnemy(battleEvent.getEnemyName(),
                battleEvent.getEnemyHP(),
                battleEvent.getEnemyAtt(),
                hero,
                battleEvent.getDbReward(),
                battleEvent.getGameProgress()
        );

        currentDBReward = battleEvent.getDbReward();
        currentExperienceReward = battleEvent.getGameProgress();

        return currentBattleEvent;
    }

    @Override
    public int getDb() {
        return currentDBReward;
    }

    @Override
    public int getGameProgress() {
        return currentExperienceReward;
    }

    @Override
    public BattleEvent createBossInstance(Hero hero) {
        currentDBReward = 177013;
        currentExperienceReward = 0;
        return new MainEnemy("Bahamut, King of Dragons", 12000, 800,
                hero,
                currentDBReward,
                currentExperienceReward
        );
    }


}
