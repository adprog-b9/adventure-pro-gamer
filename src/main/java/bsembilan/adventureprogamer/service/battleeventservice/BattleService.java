package bsembilan.adventureprogamer.service.battleeventservice;

import bsembilan.adventureprogamer.core.battleevent.template.BattleEvent;
import bsembilan.adventureprogamer.core.charactercore.gamechar.Hero;
import bsembilan.adventureprogamer.core.eventcore.eventmain.MainEvent;

public interface BattleService {

    public double Attack(float timing, MainEvent currentEvent);
    public double Defend(float timing, MainEvent currentEvent);
    public BattleEvent createNewInstance(Hero hero, BattleEvent battleEvent);
    public int getDb();
    public int getGameProgress();
    public BattleEvent createBossInstance(Hero hero);
}
