package bsembilan.adventureprogamer.service.difficultyservice;

public interface CalculationService {
    int getActionFinal(String difficulty, int doggoSpent);
    int getRewardFinal(String difficulty, int reward);
    int getBattleIntervalFinal(String difficulty);
}
