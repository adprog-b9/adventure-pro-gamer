package bsembilan.adventureprogamer.service.difficultyservice;

import bsembilan.adventureprogamer.core.difficulty.DifficultyStrategy;
import bsembilan.adventureprogamer.repository.difficultyrepository.DifficultyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CalculationServiceImpl implements CalculationService{
    @Autowired
    DifficultyRepository difficultyRepository;

    @Override
    public int getActionFinal(String difficulty, int doggobitSpent) {
        DifficultyStrategy current = difficultyRepository.findByType(difficulty);
        double calculation = current.getActionPercentage() * doggobitSpent;
        return (int) calculation;
    }

    @Override
    public int getRewardFinal(String difficulty, int reward) {
        DifficultyStrategy current = difficultyRepository.findByType(difficulty);
        double calculation = current.getRewardPercentage() * reward;
        return (int) calculation;
    }


    @Override
    public int getBattleIntervalFinal(String difficulty) {
        DifficultyStrategy current = difficultyRepository.findByType(difficulty);
        return current.getBattleInterval();
    }

}
