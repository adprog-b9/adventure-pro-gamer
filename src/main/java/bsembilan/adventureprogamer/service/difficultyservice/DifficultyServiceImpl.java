package bsembilan.adventureprogamer.service.difficultyservice;

import bsembilan.adventureprogamer.core.charactercore.gamechar.Hero;
import bsembilan.adventureprogamer.core.difficulty.DifficultyStrategy;
import bsembilan.adventureprogamer.repository.difficultyrepository.DifficultyRepository;
import bsembilan.adventureprogamer.service.characterservice.HeroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DifficultyServiceImpl implements DifficultyService{

    @Autowired
    DifficultyRepository difficultyRepository;

    @Autowired
    private HeroService heroService;

    public DifficultyServiceImpl(DifficultyRepository difficultyRepository){
        this.difficultyRepository = difficultyRepository;
    }

    @Override
    public DifficultyStrategy findByType(String type){
        return difficultyRepository.findByType(type);
    }
    @Override
    public Iterable<DifficultyStrategy> findAll(){
        return difficultyRepository.findAll();
    }

    @Override
    public void changeStrategy(String type){
        Hero hero = heroService.getHero();
        hero.setDifficulty(type);
    }
}
