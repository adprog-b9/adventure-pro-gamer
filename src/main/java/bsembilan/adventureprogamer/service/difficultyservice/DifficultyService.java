package bsembilan.adventureprogamer.service.difficultyservice;

import bsembilan.adventureprogamer.core.difficulty.DifficultyStrategy;

public interface DifficultyService {
    DifficultyStrategy findByType(String type);
    void changeStrategy(String type);
    Iterable<DifficultyStrategy> findAll();
}
