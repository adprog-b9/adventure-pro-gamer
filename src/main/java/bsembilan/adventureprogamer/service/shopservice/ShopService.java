package bsembilan.adventureprogamer.service.shopservice;

import bsembilan.adventureprogamer.core.shopcore.Item;

import java.util.*;

public interface ShopService {
    List<String> getAllShopLogs();

    List<Item> getAllItems();

    //Maybe the parameter
    void buy(String type);

    Item getItem(String type);
}
