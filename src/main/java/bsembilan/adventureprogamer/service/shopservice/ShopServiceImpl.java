package bsembilan.adventureprogamer.service.shopservice;

import bsembilan.adventureprogamer.core.charactercore.gamechar.Hero;
import bsembilan.adventureprogamer.core.shopcore.Item;
import bsembilan.adventureprogamer.repository.shoprepository.ShopRepository;
import bsembilan.adventureprogamer.service.characterservice.HeroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ShopServiceImpl implements ShopService {

    @Autowired
    private ShopRepository shopRepository;

    @Autowired
    HeroService heroService;

    @Override
    public List<String> getAllShopLogs() {
        return shopRepository.getShopLog();
    }

    @Override
    public List<Item> getAllItems() {
        return shopRepository.getItems();
    }

    @Override
    public void buy(String type) {
        Item item = shopRepository.getItemByType(type);
        Hero hero = heroService.getHero();
        item.purchaseItem(hero);
        shopRepository.addShopLog(item.getLog());
    }

    @Override
    public Item getItem(String type) {
        return shopRepository.getItemByType(type);
    }
}
