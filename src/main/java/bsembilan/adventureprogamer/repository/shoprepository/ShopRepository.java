package bsembilan.adventureprogamer.repository.shoprepository;

import bsembilan.adventureprogamer.core.shopcore.Item;

import java.util.*;

public interface ShopRepository {

    List<String> getShopLog();

    List<Item> getItems();

    void addShopLog(String log);

    void addItem(Item item);

    Item getItemByType(String type);
}
