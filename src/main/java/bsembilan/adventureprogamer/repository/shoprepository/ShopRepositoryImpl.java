package bsembilan.adventureprogamer.repository.shoprepository;

import org.springframework.stereotype.Repository;
import bsembilan.adventureprogamer.core.shopcore.Item;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class ShopRepositoryImpl implements ShopRepository{

    private Map<String, Item> items = new HashMap<>();
    private List<String> shopLogs = new ArrayList<>();

    //Returns all logs
    @Override
    public List<String> getShopLog() {
        return shopLogs;
    }

    //Returns all items
    @Override
    public List<Item> getItems() {
        return new ArrayList<>(items.values());
    }

    //Add logs
    @Override
    public void addShopLog(String log) {
        shopLogs.add(log);
    }

    //Add items
    @Override
    public void addItem(Item item) {
        this.items.put(item.getType(), item);
    }

    //Get item by type
    @Override
    public Item getItemByType(String type) {
        return items.get(type);
    }
}
