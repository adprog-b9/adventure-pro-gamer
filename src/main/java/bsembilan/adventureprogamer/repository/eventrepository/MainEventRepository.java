package bsembilan.adventureprogamer.repository.eventrepository;

import bsembilan.adventureprogamer.core.eventcore.eventmain.MainEvent;

import java.util.List;

public interface MainEventRepository {
    List<MainEvent> getEventList();

    void setEventList(List<MainEvent> eventList);

    MainEvent add(MainEvent mainEvent);

    MainEvent getEvent(String eventName);

    boolean checkEvent(MainEvent mainEvent);

}
