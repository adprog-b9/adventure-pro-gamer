package bsembilan.adventureprogamer.repository.eventrepository;


import bsembilan.adventureprogamer.core.eventcore.eventmain.MainEvent;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.ArrayList;

@Repository
public class MainEventRepositoryImpl implements MainEventRepository {
    private List<MainEvent> eventList;

    public MainEventRepositoryImpl() {
        this.eventList = new ArrayList<>();
    }

    public List<MainEvent> getEventList() {
        return eventList;
    }

    public void setEventList(List<MainEvent> eventList) {
        this.eventList = eventList;
    }

    public boolean checkEvent(MainEvent mainEvent) {
        for (MainEvent eachEvent:
                eventList) {
            if(eachEvent.getEventName().equals(mainEvent.getEventName())) {
                return true;
                }
            }
        return false;
    }

    public MainEvent add(MainEvent mainEvent) {
        if(checkEvent(mainEvent)) {
            return null;
        }
        eventList.add(mainEvent);
        return mainEvent;
    }

    public MainEvent getEvent(String eventName) {
        for (MainEvent eachEvent:
             eventList) {
            if(eachEvent.getEventName().equals(eventName)) {
                return eachEvent;
            }
        }
        return null;
    }

}
