package bsembilan.adventureprogamer.repository.difficultyrepository;

import bsembilan.adventureprogamer.core.difficulty.DifficultyStrategy;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@Repository
public class DifficultyRepositoryImpl implements DifficultyRepository{
    private Map<String, DifficultyStrategy> difficulties = new HashMap<>();

    public DifficultyStrategy save(DifficultyStrategy savedDifficulty) {
        DifficultyStrategy existingDifficulty = difficulties.get(savedDifficulty.getType());
        if (existingDifficulty != null) {
            return existingDifficulty;
        }
        difficulties.put(savedDifficulty.getType(),savedDifficulty);
        return difficulties.get(savedDifficulty.getType());
    }

    public DifficultyStrategy findByType(String type) {
        return difficulties.get(type);
    }

    public Iterable<DifficultyStrategy> findAll(){
        return new ArrayList<>(difficulties.values());
    }
}
