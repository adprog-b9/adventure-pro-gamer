package bsembilan.adventureprogamer.repository.difficultyrepository;

import bsembilan.adventureprogamer.core.difficulty.DifficultyStrategy;

public interface DifficultyRepository {
    DifficultyStrategy save(DifficultyStrategy savedDifficulty);
    DifficultyStrategy findByType(String type);
    Iterable<DifficultyStrategy> findAll();
}
