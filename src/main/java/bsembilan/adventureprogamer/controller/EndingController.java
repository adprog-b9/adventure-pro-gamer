package bsembilan.adventureprogamer.controller;


import bsembilan.adventureprogamer.core.battleevent.adapter.BattleEventAdapter;
import bsembilan.adventureprogamer.service.battleeventservice.BattleService;
import bsembilan.adventureprogamer.service.characterservice.HeroService;
import bsembilan.adventureprogamer.service.maineventservice.MainEventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(path = "/theEnd")
public class EndingController {

    @Autowired
    MainEventService mainEventService;

    @Autowired
    HeroService heroService;

    @Autowired
    BattleService battleEventService;


    @GetMapping(path = "/danger")
    public String dangerAhead() {

        if(!mainEventService.getIsTheEnd()) {
            return "redirect:/";
        }

        return "event/dangerahead";
    }

    @GetMapping(path = "/bahamut")
    public String finalBattle(Model model) {

        if(!mainEventService.getIsTheEnd()) {
            return "redirect:/";
        }

        var newInstanceBossEvent = battleEventService.createBossInstance(heroService.getHero());
        var currentEvent = new BattleEventAdapter(newInstanceBossEvent);

        currentEvent.setDescription("Bahamut: \"You dare overstep your mortal authority!?\"");
        model.addAttribute("event", currentEvent);
        model.addAttribute("battleInterval", 1);
        model.addAttribute("isTheEnd", mainEventService.getIsTheEnd());
        return "event/battleevent";

    }

}
