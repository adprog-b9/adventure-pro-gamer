package bsembilan.adventureprogamer.controller;


import bsembilan.adventureprogamer.service.characterservice.HeroService;
import bsembilan.adventureprogamer.service.maineventservice.MainEventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class LobbyController {

    @Autowired
    HeroService heroService;

    @Autowired
    MainEventService mainEventService;

    @GetMapping(path = "/mainlobby")
    public String mainLobby(Model model){

        if (heroService.getHero().getName() == null) {
            return "redirect:/";
        }

        mainEventService.clearOngoingEvent();
        model.addAttribute("heroInstance", heroService.getHero());
        model.addAttribute("heroStats", heroService.getHeroCharStats());
        return "lobby/lobby";
    }

    @PostMapping(path = "/deleteHero")
    public String removeCharacter() {
        heroService.resetCharInstance();
        mainEventService.setIsTheEnd(false);
        return "lobby/deleted";
    }

}
