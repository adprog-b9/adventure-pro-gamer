package bsembilan.adventureprogamer.controller;

import bsembilan.adventureprogamer.service.characterservice.HeroService;
import bsembilan.adventureprogamer.service.shopservice.ShopService;
import io.micrometer.core.annotation.Timed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(path = "/shop")
public class ShopController {

    @Autowired
    private ShopService shopService;

    @Autowired
    private HeroService heroService;

    @Timed("shopHome")
    @GetMapping(path = "")
    public String shopHome(Model model) {
        model.addAttribute("items", shopService.getAllItems());
        model.addAttribute("logs", shopService.getAllShopLogs());
        model.addAttribute("hero", heroService.getHero());
        return "shop/shop";
    }

    @PostMapping(path = "/buy")
    public String buyItem(
            @RequestParam(value = "itemType") String itemType) {
        shopService.buy(itemType);
        return "redirect:/shop";
    }
}
