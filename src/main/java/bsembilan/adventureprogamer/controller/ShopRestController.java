package bsembilan.adventureprogamer.controller;

import bsembilan.adventureprogamer.core.shopcore.Item;
import bsembilan.adventureprogamer.service.shopservice.ShopService;
import io.micrometer.core.annotation.Timed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/shopAPI")
public class ShopRestController {
    @Autowired
    private ShopService shopService;

    @GetMapping(path = "/{itemType}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Item> currentItem(
            @PathVariable(value = "itemType") String itemType) {
        var item = shopService.getItem(itemType);
        return ResponseEntity.ok(item);
    }


    @Timed("buyItem")
    @PostMapping(path = "/buy/{itemType}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Item> buyItem(
            @PathVariable(value = "itemType") String itemType) {
        var item = shopService.getItem(itemType);
        shopService.buy(itemType);
        return ResponseEntity.ok(item);
    }
}
