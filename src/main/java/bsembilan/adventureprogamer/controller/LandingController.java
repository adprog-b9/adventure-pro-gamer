package bsembilan.adventureprogamer.controller;

import bsembilan.adventureprogamer.service.characterservice.HeroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class LandingController {

    @Autowired
    HeroService heroService;

    @GetMapping(path ="/")
    public String landing() {
        return "landing/landingPage";
    }

    @GetMapping(path ="/story")
    public String storyLine() {
        return "landing/storyline";
    }

    @GetMapping(path = "/confirmation")
    public String confirmation() {
        if (heroService.getHero().getName()!=null){
            return "landing/confirmation";
        }
        return "redirect:/story";
    }
}
