package bsembilan.adventureprogamer.controller;


import bsembilan.adventureprogamer.service.characterservice.HeroService;
import io.micrometer.core.annotation.Timed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;


@Controller
public class CreateCharacterController {


    private final HeroService heroService;

    /**
     * Constructor for battlecontroller.
     * @param heroService is for connecting with hero.
     */
    public CreateCharacterController(HeroService heroService) {
        this.heroService = heroService;
    }

    /**
     * Function for accessing the battle page naturally.
     * @param model is the models for mvc.
     * @return returns a view.
     */


    @GetMapping(path = "/character")
    public String createCharacterHome(Model model) {
        return "character/create-character";
    }

    /**
     * Function that is called after clicking the create character button.
     * @return returns a view.
     */
    @Timed("producechar")
    @PostMapping(path = "/produce")
    public String produceChar(HttpServletRequest request) {
        String heroName = request.getParameter("heroName");
        String role = request.getParameter("role");

        heroService.createChar(heroName,role,"Normal");
        return "redirect:/difficulty/";
    }
}