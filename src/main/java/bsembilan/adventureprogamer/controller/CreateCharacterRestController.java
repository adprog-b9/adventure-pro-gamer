package bsembilan.adventureprogamer.controller;

import bsembilan.adventureprogamer.core.charactercore.gamechar.Hero;
import bsembilan.adventureprogamer.service.characterservice.HeroService;
import io.micrometer.core.annotation.Timed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/characterAPI")
public class CreateCharacterRestController {
    @Autowired
    private HeroService heroService;

    @Timed("createCharacterRest")
    @PostMapping(path = "/create/produce/{heroName}/{role}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Hero> createCharacter(
            @PathVariable(value = "heroName") String heroName,
            @PathVariable(value = "role") String role
    ) {
        heroService.resetCharInstance();
        heroService.createChar(heroName, role, "Normal");
        var hero = heroService.getHero();
        return ResponseEntity.ok(hero);
    }
}
