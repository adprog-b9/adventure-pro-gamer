package bsembilan.adventureprogamer.controller;

import bsembilan.adventureprogamer.service.difficultyservice.DifficultyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import io.micrometer.core.annotation.Timed;

@Controller
@RequestMapping(path = "/difficulty")
public class DifficultyController {
    @Autowired
    private DifficultyService difficultyService;

    @Timed("getAllDescriptions")
    @RequestMapping(path = "/", method = RequestMethod.GET)
    public String getAllDescriptions(Model model){
        model.addAttribute("DaBaby", difficultyService.findByType("DaBaby"));
        model.addAttribute("Normal", difficultyService.findByType("Normal"));
        model.addAttribute("Pacil", difficultyService.findByType("Pacil"));
        return "difficulty/difficulty";
    }

    @Timed("chooseDifficulty")
    @RequestMapping(path = "/choose", method = RequestMethod.POST)
    public String chooseDifficulty(
            @RequestParam(value = "difficultySelected") String difficulty){
        difficultyService.changeStrategy(difficulty);
        return "redirect:/mainlobby";
    }

}
