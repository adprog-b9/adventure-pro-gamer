package bsembilan.adventureprogamer.controller;

import bsembilan.adventureprogamer.core.eventcore.action.Action;
import bsembilan.adventureprogamer.core.eventcore.eventmain.MainEvent;
import bsembilan.adventureprogamer.service.characterservice.HeroService;
import bsembilan.adventureprogamer.service.difficultyservice.CalculationService;
import bsembilan.adventureprogamer.service.maineventservice.MainEventService;
import io.micrometer.core.annotation.Timed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/eventAPI")
public class EventRestController {

    @Autowired
    MainEventService mainEventService;

    @Autowired
    HeroService heroService;

    @Autowired
    CalculationService calculationService;

    @GetMapping(path = "/{eventName}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<MainEvent> currentEvent(@PathVariable(value = "eventName") String eventName) {
        var decodedEvent = mainEventService.decodeEventName(eventName);
        var currentEvent = mainEventService.getEvent(decodedEvent);
        if (currentEvent == null) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else
            return ResponseEntity.ok(currentEvent);
    }

    @Timed("chooseAction")
    @PostMapping(path = "/{eventName}/{choosenAction}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Action> actionReward(@PathVariable(value = "eventName") String eventName,
                                               @PathVariable(value = "choosenAction") int chooseAction) {

        var currentHero = heroService.getHero();
        var currentHeroStats = heroService.getHeroCharStats();

        var decodedEvent = mainEventService.decodeEventName(eventName);
        var currentEvent = mainEventService.getEvent(decodedEvent);


        if (currentEvent == null) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        if(currentEvent.getHasChoosen()) {
            return new ResponseEntity<>(HttpStatus.TOO_MANY_REQUESTS);
        }

        var choosenAction = mainEventService.chooseAction(currentEvent, chooseAction);

        Integer doggoReward = calculationService.getActionFinal(currentHero.getDifficulty(),choosenAction.getDoggoReward());
        int gameProgress = calculationService.getRewardFinal(currentHero.getDifficulty(), choosenAction.getProgressReward());
        currentHeroStats.addDB(doggoReward);
        currentHeroStats.addExperience(gameProgress);

        mainEventService.setChoosen(currentEvent, true);
        mainEventService.clearOngoingEvent();
        return ResponseEntity.ok(choosenAction);
    }







}
