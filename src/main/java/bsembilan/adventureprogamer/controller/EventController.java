package bsembilan.adventureprogamer.controller;

import bsembilan.adventureprogamer.core.battleevent.adapter.BattleEventAdapter;
import bsembilan.adventureprogamer.core.eventcore.eventmain.MainEvent;
import bsembilan.adventureprogamer.service.battleeventservice.BattleService;
import bsembilan.adventureprogamer.service.characterservice.HeroService;
import bsembilan.adventureprogamer.service.difficultyservice.CalculationService;
import bsembilan.adventureprogamer.service.maineventservice.MainEventService;
import io.micrometer.core.annotation.Timed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;


@Controller
@RequestMapping(path = "/event")
public class EventController {

    @Autowired
    private MainEventService mainEventService;

    @Autowired
    private BattleService battleEventService;

    @Autowired
    private CalculationService calculationService;

    @Autowired
    private HeroService heroService;

    private MainEvent currentEvent;

    private static final String EVENTMODEL = "event";
    private static final String BATTLEEVENTTEMPLATE = "event/battleevent";


    // controller to get randomized event
    @Timed("randomEvent")
    @GetMapping(path = "/check")
    public String randomEvent(Model model){
        var currentHero = heroService.getHero();
        var currentHeroStats = currentHero.getCharStats();

        if(currentHeroStats.getExperience() > 70) {
            currentEvent = new BattleEventAdapter(battleEventService.createBossInstance(currentHero));
            mainEventService.setIsTheEnd(true);
            return "redirect:/theEnd/danger";
        }

        if(currentHero.getName() == null) {
            return "redirect:/";
        }

        var randomizedEvent = mainEventService.getRandomEvent();

        if (randomizedEvent.getType().equals("Battle")) {
            var newInstanceBattleEvent =
                    battleEventService.createNewInstance(currentHero, randomizedEvent.getBattleEvent());
            currentEvent = new BattleEventAdapter(newInstanceBattleEvent);
            var battleInterval = calculationService.getBattleIntervalFinal(currentHero.getDifficulty());

            model.addAttribute(EVENTMODEL, currentEvent);
            model.addAttribute("battleInterval", battleInterval);
            model.addAttribute("isTheEnd", mainEventService.getIsTheEnd());
            return BATTLEEVENTTEMPLATE;
        }

        currentEvent = randomizedEvent;
        model.addAttribute("randomEvent", randomizedEvent);
        model.addAttribute("ActionMin", randomizedEvent.getMinAction());
        model.addAttribute("ActionMed", randomizedEvent.getMedAction());
        model.addAttribute("ActionMax", randomizedEvent.getMaxAction());

        currentEvent.setHasChoosen(false);
        return "event/event";
    }

    @GetMapping(path = "/addEventForm")
    public String addEventForm() {
        return "event/addevent";
    }

    @PostMapping(path = "/addEvent")
    public String addEvent(HttpServletRequest request) {

        String eventName = request.getParameter("eventName");
        String eventDescription = request.getParameter("eventDescription");
        String eventType = request.getParameter("eventType");

        String minDes = request.getParameter("mindes");
        String minRes = request.getParameter("minres");

        String medDes = request.getParameter("meddes");
        String medRes = request.getParameter("medres");

        String maxDes = request.getParameter("maxdes");
        String maxRes = request.getParameter("maxres");

        var addedEvent = mainEventService.createMainEvent(eventName, eventType, eventDescription);

        if(addedEvent == null) {
            return "event/eventexisted";
        }

        mainEventService.setAction(eventName, minDes, minRes, 0);
        mainEventService.setAction(eventName, medDes, medRes, 1);
        mainEventService.setAction(eventName, maxDes, maxRes, 2);

        return "redirect:/mainlobby";
    }

    @Timed("lose")
    @PostMapping(path="/lose")
    public ResponseEntity<HttpStatus> lose() {
        heroService.resetCharInstance();
        mainEventService.setIsTheEnd(false);
        mainEventService.clearOngoingEvent();
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Timed("win")
    @PostMapping(path="/win")
    public ResponseEntity<HttpStatus> win() {
        heroService.addDB(battleEventService.getDb());
        heroService.addExperience(battleEventService.getGameProgress());
        mainEventService.clearOngoingEvent();

        if(mainEventService.getIsTheEnd()) {
            heroService.resetCharInstance();
            mainEventService.setIsTheEnd(false);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Timed("attackPlayer")
    @PostMapping(path = "/attackEnemy")
    public ResponseEntity<Double> attackEnemy(@RequestParam(value = "timing") float timing, Model model) {
        Double enemyRemainingHP = battleEventService.Attack(timing, currentEvent);
        return new ResponseEntity<>(enemyRemainingHP, HttpStatus.OK);
    }

    @Timed("defendPlayer")
    @PostMapping(path = "/defendPlayer")
    public ResponseEntity<Double> defendPlayer(@RequestParam(value = "timing") float timing, Model model) {
        Double playerRemainingHP = battleEventService.Defend(timing, currentEvent);
        return new ResponseEntity<>(playerRemainingHP, HttpStatus.OK);
    }
}
