package bsembilan.adventureprogamer.core.difficulty;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class DaBabyStrategyTest {
    private Class<?> DaBabyStrategyClass;

    @BeforeEach
    public void setUp() throws Exception {
        DaBabyStrategyClass = Class.forName("bsembilan.adventureprogamer.core.difficulty.DaBabyStrategy");
    }

    @Test
    public void testDaBabyStrategyIsADifficultyStrategy() {
        Collection<Type> interfaces = Arrays.asList(DaBabyStrategyClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("bsembilan.adventureprogamer.core.difficulty.DifficultyStrategy")));
    }

    @Test
    public void testDaBabyStrategyOverrideGetDescriptions() throws Exception {
        Method getDescriptions = DaBabyStrategyClass.getDeclaredMethod("getDescriptions");

        assertTrue(Modifier.isPublic(getDescriptions.getModifiers()));
        assertEquals("java.lang.String",getDescriptions.getGenericReturnType().getTypeName());
    }

    @Test
    public void testDaBabyStrategyOverrideGetRewardPercentage() throws Exception {
        Method getRewardPercentage = DaBabyStrategyClass.getDeclaredMethod("getRewardPercentage");

        assertTrue(Modifier.isPublic(getRewardPercentage.getModifiers()));
        assertEquals("double",getRewardPercentage.getGenericReturnType().getTypeName());
    }

    @Test
    public void testDaBabyStrategyOverrideGetActionPercentage() throws Exception {
        Method getActionPercentage = DaBabyStrategyClass.getDeclaredMethod("getActionPercentage");

        assertTrue(Modifier.isPublic(getActionPercentage.getModifiers()));
        assertEquals("double",getActionPercentage.getGenericReturnType().getTypeName());
    }
    @Test
    public void testDaBabyStrategyOverrideGetBattleInterval() throws Exception {
        Method getBattleInterval = DaBabyStrategyClass.getDeclaredMethod("getBattleInterval");

        assertTrue(Modifier.isPublic(getBattleInterval.getModifiers()));
        assertEquals("int",getBattleInterval.getGenericReturnType().getTypeName());
    }

    @Test
    public void testDaBabyStrategyGetTypeReturnsNormal(){
        DaBabyStrategy daBaby = new DaBabyStrategy();
        assertEquals("DaBaby", daBaby.getType());
    }
}
