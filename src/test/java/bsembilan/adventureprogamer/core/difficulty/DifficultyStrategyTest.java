package bsembilan.adventureprogamer.core.difficulty;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class DifficultyStrategyTest {
    private Class<?> difficultyStrategyClass;

    @BeforeEach
    public void setUp() throws Exception {
        difficultyStrategyClass = Class.forName("bsembilan.adventureprogamer.core.difficulty.DifficultyStrategy");
    }

    @Test
    public void testDifficultyStrategyIsAPublicInterface() {
        int classModifiers = difficultyStrategyClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testDifficultyStrategyIsStrategy() {
        Collection<Type> interfaces = Arrays.asList(difficultyStrategyClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("bsembilan.adventureprogamer.core.difficulty.Strategy")));
    }

    @Test
    public void testDifficultyStrategyHasGetDecriptionsAbstractMethod() throws Exception {
        Method getDecriptions = difficultyStrategyClass.getDeclaredMethod("getDescriptions");
        int methodModifiers = getDecriptions.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, getDecriptions.getParameterCount());
        assertEquals("java.lang.String", getDecriptions.getGenericReturnType().getTypeName());
    }

    @Test
    public void testDifficultyStrategyHasGetRewardPercentageAbstractMethod() throws Exception {
        Method getRewardPercentage = difficultyStrategyClass.getDeclaredMethod("getRewardPercentage");
        int methodModifiers = getRewardPercentage.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, getRewardPercentage.getParameterCount());
        assertEquals("double", getRewardPercentage.getGenericReturnType().getTypeName());
    }

    @Test
    public void testDifficultyStrategyHasGetActionPercentageAbstractMethod() throws Exception {
        Method getActionPercentage = difficultyStrategyClass.getDeclaredMethod("getActionPercentage");
        int methodModifiers = getActionPercentage.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, getActionPercentage.getParameterCount());
        assertEquals("double", getActionPercentage.getGenericReturnType().getTypeName());
    }

    @Test
    public void testDifficultyStrategyHasGetBattleIntervalAbstractMethod() throws Exception {
        Method getBattleInterval = difficultyStrategyClass.getDeclaredMethod("getBattleInterval");
        int methodModifiers = getBattleInterval.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, getBattleInterval.getParameterCount());
        assertEquals("int", getBattleInterval.getGenericReturnType().getTypeName());
    }
}
