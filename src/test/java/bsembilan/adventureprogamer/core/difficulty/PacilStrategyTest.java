package bsembilan.adventureprogamer.core.difficulty;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PacilStrategyTest {
    private Class<?> PacilStrategyClass;

    @BeforeEach
    public void setUp() throws Exception {
        PacilStrategyClass = Class.forName("bsembilan.adventureprogamer.core.difficulty.PacilStrategy");
    }

    @Test
    public void testPacilStrategyIsADifficultyStrategy() {
        Collection<Type> interfaces = Arrays.asList(PacilStrategyClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("bsembilan.adventureprogamer.core.difficulty.DifficultyStrategy")));
    }

    @Test
    public void testPacilStrategyOverrideGetDescriptions() throws Exception {
        Method getDescriptions = PacilStrategyClass.getDeclaredMethod("getDescriptions");

        assertTrue(Modifier.isPublic(getDescriptions.getModifiers()));
        assertEquals("java.lang.String",getDescriptions.getGenericReturnType().getTypeName());
    }

    @Test
    public void testPacilStrategyOverrideGetRewardPercentage() throws Exception {
        Method getRewardPercentage = PacilStrategyClass.getDeclaredMethod("getRewardPercentage");

        assertTrue(Modifier.isPublic(getRewardPercentage.getModifiers()));
        assertEquals("double",getRewardPercentage.getGenericReturnType().getTypeName());
    }

    @Test
    public void testPacilStrategyOverrideGetActionPercentage() throws Exception {
        Method getActionPercentage = PacilStrategyClass.getDeclaredMethod("getActionPercentage");

        assertTrue(Modifier.isPublic(getActionPercentage.getModifiers()));
        assertEquals("double",getActionPercentage.getGenericReturnType().getTypeName());
    }

    @Test
    public void testPacilStrategyOverrideGetBattleInterval() throws Exception {
        Method getBattleInterval = PacilStrategyClass.getDeclaredMethod("getBattleInterval");

        assertTrue(Modifier.isPublic(getBattleInterval.getModifiers()));
        assertEquals("int",getBattleInterval.getGenericReturnType().getTypeName());
    }

    @Test
    public void testPacilStrategyGetTypeReturnsNormal() {
        PacilStrategy pacil = new PacilStrategy();
        assertEquals("Pacil", pacil.getType());
    }
}
