package bsembilan.adventureprogamer.core.difficulty;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class NormalStrategyTest {
    private Class<?> NormalStrategyClass;

    @BeforeEach
    public void setUp() throws Exception {
        NormalStrategyClass = Class.forName("bsembilan.adventureprogamer.core.difficulty.NormalStrategy");
    }

    @Test
    public void testNormalStrategyIsADifficultyStrategy() {
        Collection<Type> interfaces = Arrays.asList(NormalStrategyClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("bsembilan.adventureprogamer.core.difficulty.DifficultyStrategy")));
    }


    @Test
    public void testNormalStrategyOverrideGetDescriptions() throws Exception {
        Method getDescriptions = NormalStrategyClass.getDeclaredMethod("getDescriptions");

        assertTrue(Modifier.isPublic(getDescriptions.getModifiers()));
        assertEquals("java.lang.String",getDescriptions.getGenericReturnType().getTypeName());
    }

    @Test
    public void testNormalStrategyOverrideGetRewardPercentage() throws Exception {
        Method getRewardPercentage = NormalStrategyClass.getDeclaredMethod("getRewardPercentage");

        assertTrue(Modifier.isPublic(getRewardPercentage.getModifiers()));
        assertEquals("double",getRewardPercentage.getGenericReturnType().getTypeName());
    }

    @Test
    public void testNormalStrategyOverrideGetActionPercentage() throws Exception {
        Method getActionPercentage = NormalStrategyClass.getDeclaredMethod("getActionPercentage");

        assertTrue(Modifier.isPublic(getActionPercentage.getModifiers()));
        assertEquals("double",getActionPercentage.getGenericReturnType().getTypeName());
    }
    @Test
    public void testNormalStrategyOverrideGetBattleInterval() throws Exception {
        Method getBattleInterval = NormalStrategyClass.getDeclaredMethod("getBattleInterval");

        assertTrue(Modifier.isPublic(getBattleInterval.getModifiers()));
        assertEquals("int",getBattleInterval.getGenericReturnType().getTypeName());
    }

    @Test
    public void testNormalStrategyGetTypeReturnsNormal(){
        NormalStrategy normal = new NormalStrategy();
        assertEquals("Normal", normal.getType());
    }

}
