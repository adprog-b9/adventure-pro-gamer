package bsembilan.adventureprogamer.core.shopcore;

import bsembilan.adventureprogamer.core.charactercore.gamechar.Hero;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class GoodieEndTest {

    private Class<?> goodieEndClass;

    private Hero hero;

    @BeforeEach
    public void setUp() throws Exception {
        goodieEndClass = Class.forName("bsembilan.adventureprogamer.core.shopcore.GoodieEnd");
        hero = Hero.getInstance();
    }

    @Test
    void testGoodieEndIsAnItem() {
        Class<?> parentClass = goodieEndClass.getSuperclass();

        assertEquals("bsembilan.adventureprogamer.core.shopcore.Item",
                parentClass.getName());
    }

    @Test
    void testGoodieEndOverrideChangeDoggoBitsMethod() throws Exception {
        Class[] arg = new Class[1];
        arg[0] = Hero.class;
        Method changeDoggoBits = goodieEndClass.getDeclaredMethod("changeDoggoBits", arg);

        assertTrue(Modifier.isPublic(changeDoggoBits.getModifiers()));
        assertEquals(1, changeDoggoBits.getParameterCount());
    }

    @Test
    void testChangeDoggoBitsImplementation() throws Exception {
        GoodieEnd goodieEnd = new GoodieEnd();
        hero.getCharStats().addDB(-hero.getCharStats().getDB());
        hero.getCharStats().setMaxAttack(100);

        hero.getCharStats().addDB(900);
        goodieEnd.changeDoggoBits(hero);
        assertThat(hero.getCharStats().getDB()).isEqualTo(900);

        hero.getCharStats().addDB(100);
        goodieEnd.changeDoggoBits(hero);
        assertThat(hero.getCharStats().getDB()).isZero();
    }

    @Test
    void testGoodieEndOverrideChangeStatsMethod() throws Exception {
        Class[] arg = new Class[1];
        arg[0] = Hero.class;
        Method changeStats = goodieEndClass.getDeclaredMethod("changeStats", arg);

        assertTrue(Modifier.isPublic(changeStats.getModifiers()));
        assertEquals(1, changeStats.getParameterCount());
    }

    @Test
    void testChangeStatsImplementation() throws Exception {
        GoodieEnd goodieEnd = new GoodieEnd();
        hero.getCharStats().addDB(-hero.getCharStats().getDB());

        hero.getCharStats().addDB(900);
        goodieEnd.changeStats(hero);
        assertThat(hero.getCharStats().getExperience()).isEqualTo(20);

        hero.getCharStats().addDB(100);
        goodieEnd.changeStats(hero);
        assertThat(hero.getCharStats().getExperience()).isEqualTo(40);
    }

    @Test
    void testGoodieEndOverrideSpecialLogMethod() throws Exception {
        Method log = goodieEndClass.getDeclaredMethod("specialLog");

        assertTrue(Modifier.isPublic(log.getModifiers()));
        assertEquals(0, log.getParameterCount());
    }

    @Test
    void testSpecialLogImplementation() throws Exception {
        GoodieEnd goodieEnd = new GoodieEnd();
        goodieEnd.specialLog();

        assertThat(goodieEnd.log).isEqualTo("You bought a Goodie End");
    }
    
    @Test
    void testGetTypeReturnValue() throws Exception {
        GoodieEnd goodieEnd = new GoodieEnd();
        String type = goodieEnd.getType();

        assertThat(type).isEqualTo("Goodie End");
    }

    @Test
    void testGetDescriptionReturnValue() throws Exception {
        GoodieEnd goodieEnd = new GoodieEnd();
        String description = goodieEnd.getDescription();

        assertThat(description).isEqualTo("Increase game progress");
    }

    @Test
    void testGetPriceReturnValue() throws Exception {
        GoodieEnd goodieEnd = new GoodieEnd();
        String price = goodieEnd.getPrice();

        assertThat(price).isEqualTo("1000 db");
    }

    @Test
    void testGetLogReturnValue() throws Exception {
        GoodieEnd goodieEnd = new GoodieEnd();
        hero.getCharStats().addDB(-hero.getCharStats().getDB());
        hero.getCharStats().addDB(90);

        goodieEnd.log(hero);
        assertThat(goodieEnd.getLog()).isEqualTo("Your Doggo Bits are not enough!");
    }

    @Test
    void testGetIntPriceReturnValue() throws Exception {
        GoodieEnd goodieEnd = new GoodieEnd();
        Integer intPrice = goodieEnd.getIntPrice();
        assertThat(intPrice).isEqualTo(1000);
    }
}
