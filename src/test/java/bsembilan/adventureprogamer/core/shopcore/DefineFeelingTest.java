package bsembilan.adventureprogamer.core.shopcore;

import bsembilan.adventureprogamer.core.charactercore.gamechar.Hero;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class DefineFeelingTest {

    private Class<?> defineFeelingClass;
    
    private Hero hero;

    @BeforeEach
    public void setUp() throws Exception {
        defineFeelingClass = Class.forName("bsembilan.adventureprogamer.core.shopcore.DefineFeeling");
        hero = Hero.getInstance();
    }

    @Test
    void testDefineFeelingIsAnItem() {
        Class<?> parentClass = defineFeelingClass.getSuperclass();

        assertEquals("bsembilan.adventureprogamer.core.shopcore.Item",
                parentClass.getName());
    }

    @Test
    void testDefineFeelingOverrideChangeDoggoBitsMethod() throws Exception {
        Class[] arg = new Class[1];
        arg[0] = Hero.class;
        Method changeDoggoBits = defineFeelingClass.getDeclaredMethod("changeDoggoBits", arg);

        assertTrue(Modifier.isPublic(changeDoggoBits.getModifiers()));
        assertEquals(1, changeDoggoBits.getParameterCount());
    }

    @Test
    void testChangeDoggoBitsImplementation() throws Exception {
        DefineFeeling defineFeeling = new DefineFeeling();
        hero.getCharStats().addDB(-hero.getCharStats().getDB());
        hero.getCharStats().setMaxAttack(100);

        hero.getCharStats().addDB(90);
        defineFeeling.changeDoggoBits(hero);
        assertThat(hero.getCharStats().getDB()).isEqualTo(90);

        hero.getCharStats().addDB(10);
        defineFeeling.changeDoggoBits(hero);
        assertThat(hero.getCharStats().getDB()).isZero();
    }
    
    @Test
    void testDefineFeelingOverrideChangeStatsMethod() throws Exception {
        Class[] arg = new Class[1];
        arg[0] = Hero.class;
        Method changeStats = defineFeelingClass.getDeclaredMethod("changeStats", arg);

        assertTrue(Modifier.isPublic(changeStats.getModifiers()));
        assertEquals(1, changeStats.getParameterCount());
    }

    @Test
    void testChangeStatsImplementation() throws Exception {
        DefineFeeling defineFeeling = new DefineFeeling();
        hero.getCharStats().addDB(-hero.getCharStats().getDB());
        hero.getCharStats().setMaxDefense(100);

        hero.getCharStats().addDB(90);
        defineFeeling.changeStats(hero);
        assertThat(hero.getCharStats().getMaxDefense()).isEqualTo(100);

        hero.getCharStats().addDB(10);
        defineFeeling.changeStats(hero);
        assertThat(hero.getCharStats().getMaxDefense()).isEqualTo(120);
    }

    @Test
    void testDefineFeelingOverrideSpecialLogMethod() throws Exception {
        Method log = defineFeelingClass.getDeclaredMethod("specialLog");

        assertTrue(Modifier.isPublic(log.getModifiers()));
        assertEquals(0, log.getParameterCount());
    }

    @Test
    void testSpecialLogImplementation() throws Exception {
        DefineFeeling defineFeeling = new DefineFeeling();
        defineFeeling.specialLog();

        assertThat(defineFeeling.log).isEqualTo("You bought a Define Feeling");
    }

    @Test
    void testGetTypeReturnValue() throws Exception {
        DefineFeeling defineFeeling = new DefineFeeling();
        String type = defineFeeling.getType();

        assertThat(type).isEqualTo("Define Feeling");
    }

    @Test
    void testGetDescriptionReturnValue() throws Exception {
        DefineFeeling defineFeeling = new DefineFeeling();
        String description = defineFeeling.getDescription();

        assertThat(description).isEqualTo("Increase DEF stat");
    }

    @Test
    void testGetPriceReturnValue() throws Exception {
        DefineFeeling defineFeeling = new DefineFeeling();
        String price = defineFeeling.getPrice();

        assertThat(price).isEqualTo("100 db");
    }

    @Test
    void testGetLogReturnValue() throws Exception {
        DefineFeeling defineFeeling = new DefineFeeling();
        hero.getCharStats().addDB(-hero.getCharStats().getDB());
        hero.getCharStats().addDB(90);

        defineFeeling.log(hero);
        assertThat(defineFeeling.getLog()).isEqualTo("Your Doggo Bits are not enough!");
    }

    @Test
    void testGetIntPriceReturnValue() throws Exception {
        DefineFeeling defineFeeling = new DefineFeeling();
        Integer intPrice = defineFeeling.getIntPrice();
        assertThat(intPrice).isEqualTo(100);
    }
}
