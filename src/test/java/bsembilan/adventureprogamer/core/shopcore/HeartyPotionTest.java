package bsembilan.adventureprogamer.core.shopcore;

import bsembilan.adventureprogamer.core.charactercore.gamechar.Hero;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class HeartyPotionTest {

    private Class<?> heartyPotionClass;
    
    private Hero hero;

    @BeforeEach
    public void setUp() throws Exception {
        heartyPotionClass = Class.forName("bsembilan.adventureprogamer.core.shopcore.HeartyPotion");
        hero = Hero.getInstance();
    }

    @Test
    void testHeartyPotionIsAnItem() {
        Class<?> parentClass = heartyPotionClass.getSuperclass();

        assertEquals("bsembilan.adventureprogamer.core.shopcore.Item",
                parentClass.getName());
    }

    @Test
    void testHeartyPotionOverrideChangeDoggoBitsMethod() throws Exception {
        Class[] arg = new Class[1];
        arg[0] = Hero.class;
        Method changeDoggoBits = heartyPotionClass.getDeclaredMethod("changeDoggoBits", arg);

        assertTrue(Modifier.isPublic(changeDoggoBits.getModifiers()));
        assertEquals(1, changeDoggoBits.getParameterCount());
    }

    @Test
    void testChangeDoggoBitsImplementation() throws Exception {
        HeartyPotion heartyPotion = new HeartyPotion();
        hero.getCharStats().addDB(-hero.getCharStats().getDB());
        hero.getCharStats().setMaxAttack(100);

        hero.getCharStats().addDB(90);
        heartyPotion.changeDoggoBits(hero);
        assertThat(hero.getCharStats().getDB()).isEqualTo(90);

        hero.getCharStats().addDB(10);
        heartyPotion.changeDoggoBits(hero);
        assertThat(hero.getCharStats().getDB()).isZero();
    }

    @Test
    void testHeartyPotionOverrideChangeStatsMethod() throws Exception {
        Class[] arg = new Class[1];
        arg[0] = Hero.class;
        Method changeStats = heartyPotionClass.getDeclaredMethod("changeStats", arg);

        assertTrue(Modifier.isPublic(changeStats.getModifiers()));
        assertEquals(1, changeStats.getParameterCount());
    }

    @Test
    void testChangeStatsImplementation() throws Exception {
        HeartyPotion heartyPotion = new HeartyPotion();
        hero.getCharStats().addDB(-hero.getCharStats().getDB());
        hero.getCharStats().setMaxHealth(100);

        hero.getCharStats().addDB(90);
        heartyPotion.changeStats(hero);
        assertThat(hero.getCharStats().getMaxHealth()).isEqualTo(100);

        hero.getCharStats().addDB(10);
        heartyPotion.changeStats(hero);
        assertThat(hero.getCharStats().getMaxHealth()).isEqualTo(150);
    }

    @Test
    void testHeartyPotionOverrideSpecialLogMethod() throws Exception {
        Method log = heartyPotionClass.getDeclaredMethod("specialLog");

        assertTrue(Modifier.isPublic(log.getModifiers()));
        assertEquals(0, log.getParameterCount());
    }

    @Test
    void testSpecialLogImplementation() throws Exception {
        HeartyPotion heartyPotion = new HeartyPotion();
        heartyPotion.specialLog();

        assertThat(heartyPotion.log).isEqualTo("You bought a Hearty Potion");
    }

    @Test
    void testGetTypeReturnValue() throws Exception {
        HeartyPotion heartyPotion = new HeartyPotion();
        String type = heartyPotion.getType();

        assertThat(type).isEqualTo("Hearty Potion");
    }

    @Test
    void testGetDescriptionReturnValue() throws Exception {
        HeartyPotion heartyPotion = new HeartyPotion();
        String description = heartyPotion.getDescription();

        assertThat(description).isEqualTo("Increase Max HP stat");
    }

    @Test
    void testGetPriceReturnValue() throws Exception {
        HeartyPotion heartyPotion = new HeartyPotion();
        String price = heartyPotion.getPrice();

        assertThat(price).isEqualTo("100 db");
    }

    @Test
    void testGetLogReturnValue() throws Exception {
        HeartyPotion heartyPotion = new HeartyPotion();
        hero.getCharStats().addDB(-hero.getCharStats().getDB());
        hero.getCharStats().addDB(90);

        heartyPotion.log(hero);
        assertThat(heartyPotion.getLog()).isEqualTo("Your Doggo Bits are not enough!");
    }

    @Test
    void testGetIntPriceReturnValue() throws Exception {
        HeartyPotion heartyPotion = new HeartyPotion();
        Integer intPrice = heartyPotion.getIntPrice();
        assertThat(intPrice).isEqualTo(100);
    }
}
