package bsembilan.adventureprogamer.core.shopcore;

import bsembilan.adventureprogamer.core.charactercore.gamechar.Hero;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ItemTest {

    private Class<?> item;

    @BeforeEach
    public void setUp() throws Exception {
        item = Class.forName("bsembilan.adventureprogamer.core.shopcore.Item");
    }

    @Test
    void testItemIsAPublicAbstract() {
        int classModifiers = item.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(Modifier.isAbstract(classModifiers));
    }

    @Test
    void testItemHasPurchaseItemMethod() throws Exception {
        Class[] arg = new Class[1];
        arg[0] = Hero.class;
        Method purchaseItem = item.getDeclaredMethod("purchaseItem", arg);
        int methodModifiers = purchaseItem.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(1, purchaseItem.getParameterCount());
    }

    @Test
    void testPurchaseItemImplementation() throws Exception {
        Hero hero = Hero.getInstance();
        hero.getCharStats().addDB(-hero.getCharStats().getDB());
        hero.getCharStats().addDB(100);
        hero.getCharStats().setMaxAttack(100);

        AdventTusk item = new AdventTusk();
        item.purchaseItem(hero);

        assertEquals(0, hero.getCharStats().getDB());
        assertEquals(120, hero.getCharStats().getMaxAttack());
    }

    @Test
    void testItemHasChangeDoggoBitsAbstractMethod() throws Exception {
        Class[] arg = new Class[1];
        arg[0] = Hero.class;
        Method changeDoggoBits = item.getDeclaredMethod("changeDoggoBits", arg);
        int methodModifiers = changeDoggoBits.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(1, changeDoggoBits.getParameterCount());
    }

    @Test
    void testItemHasChangeStatsAbstractMethod() throws Exception {
        Class[] arg = new Class[1];
        arg[0] = Hero.class;
        Method changeStats = item.getDeclaredMethod("changeStats", arg);
        int methodModifiers = changeStats.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(1, changeStats.getParameterCount());
    }

    @Test
    void testItemHasSpecialLogAbstractMethod() throws Exception {
        Method specialLog = item.getDeclaredMethod("specialLog");
        int methodModifiers = specialLog.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, specialLog.getParameterCount());
    }

    @Test
    void testItemHasGetTypeAbstractMethod() throws Exception {
        Method type = item.getDeclaredMethod("getType");
        int methodModifiers = type.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, type.getParameterCount());
        assertEquals("java.lang.String", type.getGenericReturnType().getTypeName());
    }

    @Test
    void testItemHasGetDescriptionAbstractMethod() throws Exception {
        Method description = item.getDeclaredMethod("getDescription");
        int methodModifiers = description.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, description.getParameterCount());
        assertEquals("java.lang.String", description.getGenericReturnType().getTypeName());
    }

    @Test
    void testItemHasGetPriceAbstractMethod() throws Exception {
        Method price = item.getDeclaredMethod("getPrice");
        int methodModifiers = price.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, price.getParameterCount());
        assertEquals("java.lang.String", price.getGenericReturnType().getTypeName());
    }
}
