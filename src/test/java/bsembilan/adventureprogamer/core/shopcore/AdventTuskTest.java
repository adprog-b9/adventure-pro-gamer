package bsembilan.adventureprogamer.core.shopcore;

import bsembilan.adventureprogamer.core.charactercore.gamechar.Hero;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class AdventTuskTest {

    private Class<?> adventTuskClass;

    private Hero hero;

    @BeforeEach
    public void setUp() throws Exception {
        adventTuskClass = Class.forName("bsembilan.adventureprogamer.core.shopcore.AdventTusk");
        hero = Hero.getInstance();
    }

    @Test
    void testAdventTuskIsConcreteClass() {
        assertFalse(Modifier.isAbstract(adventTuskClass.getModifiers()));
    }

    @Test
    void testAdventTuskIsAnItem() {
        Class<?> parentClass = adventTuskClass.getSuperclass();

        assertEquals("bsembilan.adventureprogamer.core.shopcore.Item",
                parentClass.getName());
    }

    @Test
    void testAdventTuskOverrideChangeDoggoBitsMethod() throws Exception {
        Class[] arg = new Class[1];
        arg[0] = Hero.class;
        Method changeDoggoBits = adventTuskClass.getDeclaredMethod("changeDoggoBits", arg);

        assertTrue(Modifier.isPublic(changeDoggoBits.getModifiers()));
        assertEquals(1, changeDoggoBits.getParameterCount());
    }

    @Test
    void testChangeDoggoBitsImplementation() throws Exception {
        AdventTusk adventTusk = new AdventTusk();
        hero.getCharStats().addDB(-hero.getCharStats().getDB());
        hero.getCharStats().setMaxAttack(100);

        hero.getCharStats().addDB(90);
        adventTusk.changeDoggoBits(hero);
        assertThat(hero.getCharStats().getDB()).isEqualTo(90);

        hero.getCharStats().addDB(10);
        adventTusk.changeDoggoBits(hero);
        assertThat(hero.getCharStats().getDB()).isZero();
    }

    @Test
    void testAdventTuskOverrideChangeStatsMethod() throws Exception {
        Class[] arg = new Class[1];
        arg[0] = Hero.class;
        Method changeStats = adventTuskClass.getDeclaredMethod("changeStats", arg);

        assertTrue(Modifier.isPublic(changeStats.getModifiers()));
        assertEquals(1, changeStats.getParameterCount());
    }

    @Test
    void testChangeStatsImplementation() throws Exception {
        AdventTusk adventTusk = new AdventTusk();
        hero.getCharStats().addDB(-hero.getCharStats().getDB());
        hero.getCharStats().setMaxAttack(100);

        hero.getCharStats().addDB(90);
        adventTusk.changeStats(hero);
        assertThat(hero.getCharStats().getMaxAttack()).isEqualTo(100);

        hero.getCharStats().addDB(10);
        adventTusk.changeStats(hero);
        assertThat(hero.getCharStats().getMaxAttack()).isEqualTo(120);
    }

    @Test
    void testAdventTuskOverrideSpecialLogMethod() throws Exception {
        Method log = adventTuskClass.getDeclaredMethod("specialLog");

        assertTrue(Modifier.isPublic(log.getModifiers()));
        assertEquals(0, log.getParameterCount());
    }

    @Test
    void testSpecialLogImplementation() throws Exception {
        AdventTusk adventTusk = new AdventTusk();
        adventTusk.specialLog();

        assertThat(adventTusk.log).isEqualTo("You bought an Advent Tusk");
    }

    @Test
    void testGetTypeReturnValue() throws Exception {
        AdventTusk adventTusk = new AdventTusk();
        String type = adventTusk.getType();

        assertThat(type).isEqualTo("Advent Tusk");
    }

    @Test
    void testGetDescriptionReturnValue() throws Exception {
        AdventTusk adventTusk = new AdventTusk();
        String description = adventTusk.getDescription();

        assertThat(description).isEqualTo("Increase ATK stat");
    }

    @Test
    void testGetLogReturnValue() throws Exception {
        AdventTusk adventTusk = new AdventTusk();
        hero.getCharStats().addDB(-hero.getCharStats().getDB());
        hero.getCharStats().addDB(90);

        adventTusk.log(hero);
        assertThat(adventTusk.getLog()).isEqualTo("Your Doggo Bits are not enough!");
    }

    @Test
    void testGetPriceReturnValue() throws Exception {
        AdventTusk adventTusk = new AdventTusk();
        String price = adventTusk.getPrice();

        assertThat(price).isEqualTo("100 db");
    }

    @Test
    void testGetIntPriceReturnValue() throws Exception {
        AdventTusk adventTusk = new AdventTusk();
        Integer intPrice = adventTusk.getIntPrice();
        assertThat(intPrice).isEqualTo(100);
    }
}
