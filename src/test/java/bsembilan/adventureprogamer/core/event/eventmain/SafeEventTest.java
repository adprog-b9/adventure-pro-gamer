package bsembilan.adventureprogamer.core.event.eventmain;

import bsembilan.adventureprogamer.core.eventcore.eventmain.safemain.SafeEvent;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SafeEventTest {

    SafeEvent safeEvent;

    @BeforeEach
    public void setUp() {
        safeEvent = new SafeEvent("Meeting NPC");

        safeEvent.setActionMin("aaa", "bbb");
        safeEvent.setActionMed("ccc", "ddd");
        safeEvent.setActionMax("eee", "fff");
        safeEvent.setSafeDescription("You get rewards");
    }

    @Test
    void testDangerousEventName() throws Exception {
        assertEquals("Meeting NPC", safeEvent.getEventName());
        assertEquals("Event", safeEvent.getType());
        safeEvent.setType("Something");
        assertEquals("Something", safeEvent.getType());
    }

    @Test
    void testDangerousEventDescription() throws Exception {
        assertEquals("You get rewards", safeEvent.getDescription());
    }

    @Test
    void testDangerousEventMinRewards() throws Exception {
        assertEquals("aaa", safeEvent.getMinAction().getDescription());
        assertEquals("bbb", safeEvent.getMinAction().getResultDescription());
        assertEquals(200, safeEvent.getMinAction().getDoggoReward());
        assertEquals("+200 DoggoBits", safeEvent.getMinAction().getDoggoRewardString());
        assertEquals(2, safeEvent.getMinAction().getProgressReward());
    }

    @Test
    void testDangerousEventMedRewards() throws Exception {
        assertEquals("ccc", safeEvent.getMedAction().getDescription());
        assertEquals("ddd", safeEvent.getMedAction().getResultDescription());
        assertEquals(300, safeEvent.getMedAction().getDoggoReward());
        assertEquals("+300 DoggoBits", safeEvent.getMedAction().getDoggoRewardString());
        assertEquals(3, safeEvent.getMedAction().getProgressReward());
    }

    @Test
    void testDangerousEventMaxRewards() throws Exception {
        assertEquals("eee", safeEvent.getMaxAction().getDescription());
        assertEquals("fff", safeEvent.getMaxAction().getResultDescription());
        assertEquals(500, safeEvent.getMaxAction().getDoggoReward());
        assertEquals("+500 DoggoBits", safeEvent.getMaxAction().getDoggoRewardString());
        assertEquals(4, safeEvent.getMaxAction().getProgressReward());
    }

    @Test
    void testSafeEventReturnUnsupportedOperation() {
        assertThrows(UnsupportedOperationException.class, () -> {
            safeEvent.Attack(0);
        });

        assertThrows(UnsupportedOperationException.class, () -> {
            safeEvent.Defend(0);
        });
        assertNull(safeEvent.getBattleEvent());

    }
}
