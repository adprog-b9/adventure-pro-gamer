package bsembilan.adventureprogamer.core.event.eventmain;


import bsembilan.adventureprogamer.core.eventcore.eventmain.dangerousmain.DangerousEvent;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;


class DangerousEventTest {

    DangerousEvent dangerousEvent;

    @BeforeEach
    public void setUp() {
        dangerousEvent = new DangerousEvent("Trap Event");
        dangerousEvent.setActionMin("aaa", "bbb");
        dangerousEvent.setActionMed("ccc", "ddd");
        dangerousEvent.setActionMax("eee", "fff");
        dangerousEvent.setDangerousDescription("You nearly got killed by the trap");
    }

    @Test
    void testDangerousEventName() throws Exception {
        assertEquals("Trap Event", dangerousEvent.getEventName());
        assertEquals("Event", dangerousEvent.getType());
        dangerousEvent.setType("Something");
        assertEquals("Something", dangerousEvent.getType());
    }

    @Test
    void testDangerousEventDescription() throws Exception {
        assertEquals("You nearly got killed by the trap", dangerousEvent.getDescription());
    }

    @Test
    void testDangerousEventMinRewards() throws Exception {
        assertEquals("aaa", dangerousEvent.getMinAction().getDescription());
        assertEquals("bbb", dangerousEvent.getMinAction().getResultDescription());
        assertEquals(0, dangerousEvent.getMinAction().getDoggoReward());
        assertEquals("0 DoggoBits", dangerousEvent.getMinAction().getDoggoRewardString());
        assertEquals(2, dangerousEvent.getMinAction().getProgressReward());
    }

    @Test
    void testDangerousEventMedRewards() throws Exception {
        assertEquals("ccc", dangerousEvent.getMedAction().getDescription());
        assertEquals("ddd", dangerousEvent.getMedAction().getResultDescription());
        assertEquals(-100, dangerousEvent.getMedAction().getDoggoReward());
        assertEquals("-100 DoggoBits", dangerousEvent.getMedAction().getDoggoRewardString());
        assertEquals(-1, dangerousEvent.getMedAction().getProgressReward());
    }

    @Test
    void testDangerousEventMaxRewards() throws Exception {
        assertEquals("eee", dangerousEvent.getMaxAction().getDescription());
        assertEquals("fff", dangerousEvent.getMaxAction().getResultDescription());
        assertEquals(-300, dangerousEvent.getMaxAction().getDoggoReward());
        assertEquals("-300 DoggoBits", dangerousEvent.getMaxAction().getDoggoRewardString());
        assertEquals(-5, dangerousEvent.getMaxAction().getProgressReward());
    }

    @Test
    void testDangerousEventReturnUnsupportedOperation() {
        assertThrows(UnsupportedOperationException.class, () -> {
            dangerousEvent.Attack(0);
        });

        assertThrows(UnsupportedOperationException.class, () -> {
            dangerousEvent.Defend(0);
        });
        assertNull(dangerousEvent.getBattleEvent());


    }

}
