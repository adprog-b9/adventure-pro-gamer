package bsembilan.adventureprogamer.core.event.eventmain;

import bsembilan.adventureprogamer.core.eventcore.eventmain.neutralmain.NeutralEvent;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class NeutralEventTest {

    NeutralEvent neutralEvent;

    @BeforeEach
    public void setUp() {
        neutralEvent = new NeutralEvent("Scenery Event");
        neutralEvent.setActionMin("aaa", "bbb");
        neutralEvent.setActionMed("ccc", "ddd");
        neutralEvent.setActionMax("eee", "fff");
        neutralEvent.setNeutralDescription("You see a mountain");
    }

    @Test
    void testDangerousEventName() throws Exception {
        assertEquals("Scenery Event", neutralEvent.getEventName());
        assertEquals("Event", neutralEvent.getType());
        neutralEvent.setType("Something");
        assertEquals("Something", neutralEvent.getType());
    }

    @Test
    void testDangerousEventDescription() throws Exception {
        assertEquals("You see a mountain", neutralEvent.getDescription());
    }

    @Test
    void testDangerousEventMinRewards() throws Exception {
        assertEquals("aaa", neutralEvent.getMinAction().getDescription());
        assertEquals("bbb", neutralEvent.getMinAction().getResultDescription());
        assertEquals(0, neutralEvent.getMinAction().getDoggoReward());
        assertEquals("+0 DoggoBits", neutralEvent.getMinAction().getDoggoRewardString());
        assertEquals(2, neutralEvent.getMinAction().getProgressReward());
    }

    @Test
    void testDangerousEventMedRewards() throws Exception {
        assertEquals("ccc", neutralEvent.getMedAction().getDescription());
        assertEquals("ddd", neutralEvent.getMedAction().getResultDescription());
        assertEquals(10, neutralEvent.getMedAction().getDoggoReward());
        assertEquals("+10 DoggoBits", neutralEvent.getMedAction().getDoggoRewardString());
        assertEquals(1, neutralEvent.getMedAction().getProgressReward());
    }

    @Test
    void testDangerousEventMaxRewards() throws Exception {
        assertEquals("eee", neutralEvent.getMaxAction().getDescription());
        assertEquals("fff", neutralEvent.getMaxAction().getResultDescription());
        assertEquals(50, neutralEvent.getMaxAction().getDoggoReward());
        assertEquals("+50 DoggoBits", neutralEvent.getMaxAction().getDoggoRewardString());
        assertEquals(3, neutralEvent.getMaxAction().getProgressReward());
    }

    @Test
    void testNeutralEventReturnUnsupportedOperation() {
        assertThrows(UnsupportedOperationException.class, () -> {
            neutralEvent.Attack(0);
        });

        assertThrows(UnsupportedOperationException.class, () -> {
            neutralEvent.Defend(0);
        });

        assertNull(neutralEvent.getBattleEvent());
    }
}
