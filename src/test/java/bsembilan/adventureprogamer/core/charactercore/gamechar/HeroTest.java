package bsembilan.adventureprogamer.core.charactercore.gamechar;

import static org.junit.jupiter.api.Assertions.*;

import bsembilan.adventureprogamer.core.charactercore.stats.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

 class HeroTest {

    private Class<?> heroClass;
    private Hero hero;

    /**
     * Function for repeating the instantiation of a knight before each test.
     * @throws Exception if the Class forName collects up garbage.
     */
    @BeforeEach
     void setUp() throws Exception {
        hero = Hero.getInstance();
        heroClass = Class.forName(Hero.class.getName());
        hero.createChar("Nico", "Berserker", "Normal");
    }

    @Test
     void testNoConstructors() {
        List<Constructor> constructors = Arrays.asList(heroClass.getDeclaredConstructors());

        boolean check = constructors.stream()
                .anyMatch(c -> !Modifier.isPrivate(c.getModifiers()));

        assertFalse(check);
    }

    @Test
     void testGetInstance() {
        assertEquals(hero, Hero.getInstance());
        assertEquals(hero.getClass(), heroClass);
    }

    @Test
     void testCreateChar() {
        hero.createChar("Nico", "Berserker", "Normal");
        assertEquals("Nico", hero.getName());
    }

    @Test
     void testGetCharStats() {
        assertNotNull(hero.getCharStats());
    }

    @Test
     void testSetCharStats() {
        Stats stats2 = new HeroStats("Paladin");
        hero.setCharStats(stats2);
        assertEquals(stats2, hero.getCharStats());
    }

    @Test
     void testResetCharStats() {
        Stats stats2 = hero.getCharStats();
        hero.resetCharStats();
        assertNotEquals(stats2, hero.getCharStats());
    }

    @Test
     void testGetName() {
        assertEquals("Nico", hero.getName());
    }

    @Test
     void testGetRole() {
        assertEquals("Berserker", hero.getRole());
    }

    @Test
     void testGetDifficulty() {
        assertEquals("Normal", hero.getDifficulty());
    }

    @Test
     void testSetDifficulty() {
        hero.setDifficulty("Pacil");
        assertEquals("Pacil", hero.getDifficulty());
    }

    @Test
     void testDeleteInstance() {
        hero.deleteInstance();
        assertNotEquals(hero, hero.getInstance());
    }
}