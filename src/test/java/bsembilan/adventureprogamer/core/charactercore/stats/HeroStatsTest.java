package bsembilan.adventureprogamer.core.charactercore.stats;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

 class HeroStatsTest {
    HeroStats heroStats;

    @BeforeEach
     void setUp() {
        heroStats = new HeroStats("Berserker");
    }

    @Test
     void testGetMaxHealth() {
        assertEquals(100, heroStats.getMaxHealth());
    }

    @Test
     void testGetMaxAttack() {
        assertEquals(125, heroStats.getMaxAttack());
    }

    @Test
     void testGetMaxDefense() {
        assertEquals(125, heroStats.getMaxDefense());
    }

    @Test
     void testGetExperience() {
        assertEquals(20, heroStats.getExperience());
    }

    @Test
     void testGetDB() {
        assertEquals(0, heroStats.getDB());
    }

    @Test
     void testAddDB() {
        heroStats.addDB(200);
        assertEquals(200, heroStats.getDB());
    }


    @Test
     void testSetMaxHealth() {
        heroStats.setMaxHealth(200);
        assertEquals(200, heroStats.getMaxHealth());
    }

    @Test
     void testSetMaxAttack() {
        heroStats.setMaxAttack(200);
        assertEquals(200, heroStats.getMaxAttack());
    }

    @Test
     void testSetMaxDefense() {
        heroStats.setMaxDefense(200);
        assertEquals(200, heroStats.getMaxDefense());
    }

    @Test
     void testAddHealth() {
        heroStats.addHealth(20);
        assertEquals(120, heroStats.getMaxHealth());
    }

    @Test
     void testAddExperience() {
        heroStats.addExperience(100);
        assertEquals(120, heroStats.getExperience());
    }
}
