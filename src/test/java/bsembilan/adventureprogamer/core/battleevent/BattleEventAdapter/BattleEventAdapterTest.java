package bsembilan.adventureprogamer.core.battleevent.BattleEventAdapter;

import bsembilan.adventureprogamer.core.battleevent.adapter.BattleEventAdapter;
import bsembilan.adventureprogamer.core.battleevent.template.BattleEvent;
import bsembilan.adventureprogamer.core.battleevent.template.Goblin;
import bsembilan.adventureprogamer.core.eventcore.eventmain.MainEvent;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

class BattleEventAdapterTest {
    private Class<?> battleEventAdapterClass;
    private Class<?> battleEventClass;
    private Class<?> mainEventClass;

    MainEvent battleAdapter;

    @BeforeEach
    public void setUp() throws Exception {
        battleEventAdapterClass = Class.forName("bsembilan.adventureprogamer.core.battleevent.adapter.BattleEventAdapter");
        battleEventClass = Class.forName("bsembilan.adventureprogamer.core.battleevent.template.BattleEvent");
        mainEventClass = Class.forName("bsembilan.adventureprogamer.core.eventcore.eventmain.MainEvent");

        BattleEvent goblin = new Goblin("Anak Goblin", 500, 60);
        battleAdapter = new BattleEventAdapter(goblin);
        battleAdapter.setDescription("Wahh, kamu diserang Anak Goblin.");
    }

    @Test
    void testBattleEventAdapterIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(battleEventAdapterClass.getModifiers()));
    }

    @Test
    void testBattleEventAdapterConstructorReceivesBattleEventAsParameter() {
        Class<?>[] classArg = new Class[1];
        classArg[0] = battleEventClass;
        Collection<Constructor<?>> constructors = Arrays.asList(
                battleEventAdapterClass.getDeclaredConstructors());

        assertTrue(constructors.stream()
                .anyMatch(type -> Arrays.equals(
                        type.getParameterTypes(), classArg)));
    }

    @Test
    void testBattleEventAdapterOverrideAttackMethodFromMainEvent() throws Exception {
        Method methodOnMainEvent = battleEventAdapterClass.getDeclaredMethod("Attack", float.class);

        assertEquals("double",
                methodOnMainEvent.getGenericReturnType().getTypeName());
        assertEquals(1,
                methodOnMainEvent.getParameterCount());
        assertTrue(Modifier.isPublic(methodOnMainEvent.getModifiers()));
    }

    @Test
    void testBattleEventAdapterOverrideDefendMethodFromMainEvent() throws Exception {
        Method methodOnMainEvent = battleEventAdapterClass.getDeclaredMethod("Defend", float.class);

        assertEquals("double",
                methodOnMainEvent.getGenericReturnType().getTypeName());
        assertEquals(1,
                methodOnMainEvent.getParameterCount());
        assertTrue(Modifier.isPublic(methodOnMainEvent.getModifiers()));
    }

    @Test
    void testBattleEventAdapterOverrideGetBattleEventMethodFromMainEvent() throws Exception {
        Method methodOnMainEvent = battleEventAdapterClass.getDeclaredMethod("getBattleEvent");

        assertEquals("bsembilan.adventureprogamer.core.battleevent.template.BattleEvent",
                methodOnMainEvent.getGenericReturnType().getTypeName());
        assertEquals(0,
                methodOnMainEvent.getParameterCount());
        assertTrue(Modifier.isPublic(methodOnMainEvent.getModifiers()));
    }

    @Test
    void testNeutralEventReturnUnsupportedOperation() {
        assertThrows(UnsupportedOperationException.class, () -> {
            battleAdapter.setActionMin("a", "a");
        });

        assertThrows(UnsupportedOperationException.class, () -> {
            battleAdapter.setActionMed("a", "a");
        });

        assertThrows(UnsupportedOperationException.class, () -> {
            battleAdapter.setActionMax("a", "a");
        });
    }
}
