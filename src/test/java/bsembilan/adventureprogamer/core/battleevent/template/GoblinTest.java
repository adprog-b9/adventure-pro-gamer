package bsembilan.adventureprogamer.core.battleevent.template;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

class GoblinTest {

    private Class<?> goblinClass;

    @BeforeEach
    void setUp() throws Exception {
        goblinClass = Class.forName("bsembilan.adventureprogamer.core.battleevent.template.Goblin");
   }

    @Test
    void testGoblinClassIsConcreteClass() {
        assertFalse(Modifier.isAbstract(goblinClass.getModifiers()));
    }

    @Test
    void testSuperClass() {
        Class<?> parentClass = goblinClass.getSuperclass();

        assertEquals("bsembilan.adventureprogamer.core.battleevent.template.BattleEvent",
                parentClass.getName());
    }
}
