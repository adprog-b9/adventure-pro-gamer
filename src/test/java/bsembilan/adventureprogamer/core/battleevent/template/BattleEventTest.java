package bsembilan.adventureprogamer.core.battleevent.template;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.util.ReflectionTestUtils;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class BattleEventTest {
    private Class<?> player; // To be implemented
    private Class<?> battleEventClass;
    private BattleEvent battleEvent;

    @Mock
    private String enemyName;

    @Mock
    private int enemyHP;

    @Mock
    private int enemyAtt;

    @BeforeEach
    void setup() throws Exception {

        battleEventClass = Class.forName("bsembilan.adventureprogamer.core.battleevent.template.BattleEvent");
        battleEvent =(BattleEvent)Mockito.mock(battleEventClass, Mockito.CALLS_REAL_METHODS);

        ReflectionTestUtils.setField(battleEvent, "enemyName", "test");
        ReflectionTestUtils.setField(battleEvent, "enemyHP", 100);
        ReflectionTestUtils.setField(battleEvent, "enemyAtt", 100);

        ReflectionTestUtils.setField(battleEvent, "playerHP", 100);
        ReflectionTestUtils.setField(battleEvent, "playerDef", 100);
        ReflectionTestUtils.setField(battleEvent, "playerAtt", 100);

        ReflectionTestUtils.setField(battleEvent, "dbReward", 100);
        ReflectionTestUtils.setField(battleEvent, "gameProgress", 100);
    }

    @Test
    void testBattleEventIsAPublicAbstractClass() {
        int classModifiers = battleEventClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(Modifier.isAbstract(classModifiers));
    }

    @Test
    void testBattleEventHasAttackMethod() throws Exception {
        Method attack = battleEventClass.getDeclaredMethod("Attack", float.class);
        int methodModifiers = attack.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    void testBattleEventAttackReturnsEnemyHPLeft() throws Exception {
        assertEquals(0, battleEvent.Attack(1));
    }

    @Test
    void testBattleEventHasDefendMethod() throws Exception {
        Method attack = battleEventClass.getDeclaredMethod("Defend", float.class);
        int methodModifiers = attack.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    void testBattleEventDefendReturnsPlayerHPLeft() throws Exception {
        assertEquals(100, battleEvent.Defend(1));
        assertEquals(100, battleEvent.Defend(2)); // if defense > enemy attack
        assertEquals(0, battleEvent.Defend(0));
    }

    @ParameterizedTest
    @CsvSource({"getEnemyHP", "getEnemyAtt", "getEnemyName",
        "getPlayerHP", "getPlayerAtt", "getPlayerDef"})
    void testMethodAvailable(String arg) throws Exception {
        Method method = battleEventClass.getDeclaredMethod(arg);
        int methodModifiers = method.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    void testBattleEventGetEnemyName() {
        assertEquals("test", battleEvent.getEnemyName());
    }

    @Test
    void testBattleEventGetEnemyHP() {
        assertEquals(100, battleEvent.getEnemyHP());
    }

    @Test
    void testBattleEventGetEnemyAtt() {
        assertEquals(100, battleEvent.getEnemyAtt());
    }

    @Test
    void testBattleEventGetPlayerAtt() {
        assertEquals(100, battleEvent.getPlayerAtt());
    }

    @Test
    void testBattleEventGetPlayerHP() {
        assertEquals(100, battleEvent.getPlayerHP());
    }

    @Test
    void testBattleEventGetPlayerDef() {
        assertEquals(100, battleEvent.getPlayerDef());
    }

    @Test
    void testBattleEventGetDbReward() {
        assertEquals(100, battleEvent.getDbReward());
    }

    @Test
    void testBattleEventGetGameProgress() {
        assertEquals(100, battleEvent.getGameProgress());
    }
}
