package bsembilan.adventureprogamer.core.battleevent.template;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

class DragonTest {

    private Class<?> dragonClass;

    @BeforeEach
    void setUp() throws Exception {
        dragonClass = Class.forName("bsembilan.adventureprogamer.core.battleevent.template.Dragon");
    }

    @Test
    void testDragonClassIsConcreteClass() {
        assertFalse(Modifier.isAbstract(dragonClass.getModifiers()));
    }

    @Test
    void testSuperClass() {
        Class<?> parentClass = dragonClass.getSuperclass();

        assertEquals("bsembilan.adventureprogamer.core.battleevent.template.BattleEvent",
                parentClass.getName());
    }
}
