package bsembilan.adventureprogamer.core.battleevent.template;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

class MainEnemyTest {

    private Class<?> mainEnemyClass;

    @BeforeEach
    void setUp() throws Exception {
        mainEnemyClass = Class.forName("bsembilan.adventureprogamer.core.battleevent.template.MainEnemy");
    }

    @Test
    void testMainEnemyClassIsConcreteClass() {
        assertFalse(Modifier.isAbstract(mainEnemyClass.getModifiers()));
    }

    @Test
    void testSuperClass() {
        Class<?> parentClass = mainEnemyClass.getSuperclass();

        assertEquals("bsembilan.adventureprogamer.core.battleevent.template.BattleEvent",
                parentClass.getName());
    }
}
