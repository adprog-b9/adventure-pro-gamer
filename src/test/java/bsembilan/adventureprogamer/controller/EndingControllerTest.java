package bsembilan.adventureprogamer.controller;


import bsembilan.adventureprogamer.core.battleevent.template.BattleEvent;
import bsembilan.adventureprogamer.core.battleevent.template.MainEnemy;
import bsembilan.adventureprogamer.core.charactercore.gamechar.Hero;
import bsembilan.adventureprogamer.service.battleeventservice.BattleServiceImpl;
import bsembilan.adventureprogamer.service.characterservice.HeroServiceImpl;
import bsembilan.adventureprogamer.service.maineventservice.MainEventServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = EndingController.class)
public class EndingControllerTest {

    @Autowired
    private MockMvc mockMvc;


    @MockBean
    private MainEventServiceImpl mainEventService;

    @MockBean
    private HeroServiceImpl heroService;

    @MockBean
    private BattleServiceImpl battleService;


    @Test
    void whenTheEndIsNearButNotTheEndItShouldRedirectToMainLobby() throws Exception {

        when(mainEventService.getIsTheEnd()).thenReturn(false);

        mockMvc.perform(get("/theEnd/danger"))
                .andExpect(status().isFound())
                .andExpect(handler().methodName("dangerAhead"));
    }

    @Test
    void whenTheEndIsNearButItTheEndItShouldServeDangerScreen() throws Exception {

        when(mainEventService.getIsTheEnd()).thenReturn(true);

        mockMvc.perform(get("/theEnd/danger"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("dangerAhead"))
                .andExpect(view().name("event/dangerahead"));
    }

    @Test
    void whenTheEndIsNearButNotTheEndRedirectToMainLobby() throws Exception {
        when(mainEventService.getIsTheEnd()).thenReturn(false);

        mockMvc.perform(get("/theEnd/bahamut"))
                .andExpect(status().isFound())
                .andExpect(handler().methodName("finalBattle"));
    }


    @Test
    void whenTheEndIsNearButTheEndItShouldServeBahamut() throws Exception {

        var testHero = Hero.getInstance();
        testHero.createChar("ddelvo", "Paladin", "Pacil");

        BattleEvent dragon = new MainEnemy("Bahamut", 500, 60, testHero, 200, 100);

        when(heroService.getHero()).thenReturn(testHero);
        when(mainEventService.getIsTheEnd()).thenReturn(true);
        when(battleService.createBossInstance(testHero)).thenReturn(dragon);

        mockMvc.perform(get("/theEnd/bahamut"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("finalBattle"));
    }








}
