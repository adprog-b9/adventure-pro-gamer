package bsembilan.adventureprogamer.controller;

import bsembilan.adventureprogamer.core.difficulty.*;
import bsembilan.adventureprogamer.repository.difficultyrepository.DifficultyRepository;
import bsembilan.adventureprogamer.service.difficultyservice.DifficultyService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
@WebMvcTest(controllers = DifficultyController.class)
public class DifficultyControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private DifficultyService difficultyService;

    @MockBean
    private DifficultyRepository difficultyRepository;

    DifficultyStrategy dababy;
    DifficultyStrategy normal;
    DifficultyStrategy pacil;

    @BeforeEach
    public void init(){
        dababy = new DaBabyStrategy();
        normal = new NormalStrategy();
        pacil = new PacilStrategy();
        difficultyRepository.save(dababy);
        difficultyRepository.save(normal);
        difficultyRepository.save(pacil);
    }

    @Test
    public void testWhenDifficultyURLIsAccessedItShouldContainCorrectDefaultDifficultiesModel() throws Exception {
        lenient().when(difficultyService.findByType("DaBaby")).thenReturn(dababy);
        lenient().when(difficultyService.findByType("Normal")).thenReturn(normal);
        lenient().when(difficultyService.findByType("Pacil")).thenReturn(pacil);

        mockMvc.perform(get("/difficulty/"))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("DaBaby"))
                .andExpect(model().attributeExists("Normal"))
                .andExpect(model().attributeExists("Pacil"));
        verify(difficultyService, times(1)).findByType("DaBaby");
        verify(difficultyService, times(1)).findByType("Normal");
        verify(difficultyService, times(1)).findByType("Pacil");
    }

    @Test
    public void testWhenChooseDifficultyURLIsAccessedShouldCallChangeStrategy() throws Exception {
        mockMvc.perform(post("/difficulty/choose")
                .param("difficultySelected","DaBaby"))
                .andExpect(handler().methodName("chooseDifficulty"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/mainlobby"));
        verify(difficultyService,times(1)).changeStrategy("DaBaby");
    }
}
