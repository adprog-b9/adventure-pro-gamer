package bsembilan.adventureprogamer.controller;

import bsembilan.adventureprogamer.core.charactercore.gamechar.Hero;
import bsembilan.adventureprogamer.core.shopcore.AdventTusk;
import bsembilan.adventureprogamer.core.shopcore.Item;
import bsembilan.adventureprogamer.service.characterservice.HeroServiceImpl;
import bsembilan.adventureprogamer.service.shopservice.ShopServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;


import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@WebMvcTest(controllers = ShopRestController.class)
class ShopRestControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ShopServiceImpl shopService;

    @MockBean
    private HeroServiceImpl heroService;

    private Item item;
    private Hero hero;

    @BeforeEach
    public void setUp() {
        item = new AdventTusk();
        hero = Hero.getInstance();
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    void testControllerGetItem() throws Exception{
        when(shopService.getItem("Advent Tusk")).thenReturn(item);

        mockMvc.perform(get("/shopAPI/Advent Tusk")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.log").exists())
                .andExpect(jsonPath("$.type").value("Advent Tusk"))
                .andExpect(jsonPath("$.description").value("Increase ATK stat"))
                .andExpect(jsonPath("$.price").value("100 db"))
                .andExpect(jsonPath("$.intPrice").value(100));
    }

    @Test
    void testControllerPostBuy() throws Exception{
        item.log(hero);
        when(shopService.getItem("Advent Tusk")).thenReturn(item);

        mockMvc.perform(post("/shopAPI/buy/Advent Tusk")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.log").exists())
                .andExpect(jsonPath("$.type").value("Advent Tusk"))
                .andExpect(jsonPath("$.description").value("Increase ATK stat"))
                .andExpect(jsonPath("$.price").value("100 db"))
                .andExpect(jsonPath("$.intPrice").value(100));

    }
}
