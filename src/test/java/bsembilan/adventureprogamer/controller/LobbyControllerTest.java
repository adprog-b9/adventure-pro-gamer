package bsembilan.adventureprogamer.controller;

import bsembilan.adventureprogamer.core.charactercore.gamechar.Hero;
import bsembilan.adventureprogamer.service.characterservice.HeroServiceImpl;
import bsembilan.adventureprogamer.service.maineventservice.MainEventServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = LobbyController.class)
public class LobbyControllerTest {


    @Autowired
    private MockMvc mvc;

    @MockBean
    private HeroServiceImpl heroService;

    @MockBean
    private MainEventServiceImpl mainEventService;

    private Hero testHero;

    @Test
    void whenThereIsNoCharacterRedirectToCreationScreen() throws Exception {
        testHero = Hero.getInstance();
        testHero.createChar(null, "Paladin", "Pacil");

        when(heroService.getHero()).thenReturn(testHero);

        mvc.perform(get("/mainlobby"))
                .andExpect(status().isFound())
                .andExpect(handler().methodName("mainLobby"));
    }

    @Test
    void whenThereIsCharacterServeLobbyScreen() throws Exception {
        testHero = Hero.getInstance();
        testHero.createChar("ddelvo", "Paladin", "Pacil");

        when(heroService.getHero()).thenReturn(testHero);
        when(heroService.getHeroCharStats()).thenReturn(testHero.getCharStats());

        mvc.perform(get("/mainlobby"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("mainLobby"))
                .andExpect(model().attributeExists("heroInstance"))
                .andExpect(model().attributeExists("heroStats"))
                .andExpect(view().name("lobby/lobby"));

    }

    @Test
    void whenDeleteHeroIsSelectedItShouldDeleteIt() throws Exception {

        mvc.perform(post("/deleteHero"))
                .andExpect(status().isOk())
                .andExpect(view().name("lobby/deleted"));

        verify(heroService, times(1)).resetCharInstance();

    }


}
