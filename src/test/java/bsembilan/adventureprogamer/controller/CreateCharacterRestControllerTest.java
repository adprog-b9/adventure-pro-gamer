package bsembilan.adventureprogamer.controller;

import bsembilan.adventureprogamer.core.charactercore.gamechar.Hero;

import bsembilan.adventureprogamer.service.characterservice.HeroServiceImpl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;


import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@WebMvcTest(controllers = CreateCharacterRestController.class)
public class CreateCharacterRestControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private HeroServiceImpl heroService;

    private Hero hero;

    @BeforeEach
    public void setUp() {
        hero = Hero.getInstance();
    }

    @Test
    public void testControllerPostCreateCharacter() throws Exception{
        heroService.createChar("test","Paladin","Normal");
        when(heroService.getHero()).thenReturn(hero);

        mockMvc.perform(post("/characterAPI/create/produce/test/Paladin")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());

    }
}
