package bsembilan.adventureprogamer.controller;


import bsembilan.adventureprogamer.core.charactercore.gamechar.Hero;
import bsembilan.adventureprogamer.core.eventcore.eventmain.dangerousmain.DangerousEvent;
import bsembilan.adventureprogamer.core.eventcore.eventmain.MainEvent;
import bsembilan.adventureprogamer.service.characterservice.HeroService;
import bsembilan.adventureprogamer.service.difficultyservice.CalculationService;
import bsembilan.adventureprogamer.service.maineventservice.MainEventService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = EventRestController.class)
class EventRestControllerTest {

    @Autowired
    private MockMvc mvc;


    @MockBean
    private MainEventService mainEventService;

    @MockBean
    private HeroService heroService;

    @MockBean
    private CalculationService calculationService;

    private MainEvent mainEvent;
    private Hero testHero;

    @BeforeEach
    public void setUp() {
        mainEvent = new DangerousEvent("test main event");
        mainEvent.setActionMin("abc", "cde");
        mainEvent.setActionMed("abc", "fgh");
        mainEvent.setActionMax("abc", "ijk");
        mainEvent.setDescription("aaaa");

        testHero = Hero.getInstance();
        testHero.createChar("ddelvo", "Paladin", "Pacil");


    }

    @Test
    void whenEventAPIIsCalledItShouldReturnJSONEvent() throws Exception {
        when(mainEventService.decodeEventName("test%20main%20event")).thenReturn("test main event");
        when(mainEventService.getEvent("test main event")).thenReturn(mainEvent);

        mvc.perform(get("/eventAPI/test%20main%20event").contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.description").value("aaaa"))
                .andExpect(jsonPath("$.eventName").value("test main event"));
    }

    @Test
    void whenEventAPIIsCalledButNotFoundItShouldReturnNoContent() throws Exception {
        when(mainEventService.decodeEventName("test%20main%20event")).thenReturn("test main event");
        when(mainEventService.getEvent("test main event")).thenReturn(null);

        mvc.perform(get("/eventAPI/test%20main%20event").contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNoContent());
    }

    @Test
    void whenActionAPIIsCalledItShouldReturnRewardJSON() throws Exception {
        when(heroService.getHero()).thenReturn(testHero);
        when(heroService.getHeroCharStats()).thenReturn(testHero.getCharStats());

        when(mainEventService.decodeEventName("test%20main%20event")).thenReturn("test main event");
        when(mainEventService.decodeEventName("test2%20main%20event")).thenReturn("test2 main event");

        when(mainEventService.getEvent("test main event")).thenReturn(mainEvent);
        when(mainEventService.getEvent("test2 main event")).thenReturn(null);

        when(mainEventService.chooseAction(mainEvent, 2)).thenReturn(mainEvent.getMaxAction());
        when(calculationService.getActionFinal(testHero.getDifficulty(), -300)).thenReturn(-150);
        when(calculationService.getRewardFinal(testHero.getDifficulty(), -5)).thenReturn(-7);

        mainEvent.setHasChoosen(false);

        mvc.perform(post("/eventAPI/test%20main%20event/2").contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.rewardType").value("2"))
                .andExpect(jsonPath("$.resultDescription").value("ijk"));

        mainEvent.setHasChoosen(true);

        mvc.perform(post("/eventAPI/test%20main%20event/2").contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isTooManyRequests());

        mvc.perform(post("/eventAPI/test2%20main%20event/2").contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNoContent());

    }


}
