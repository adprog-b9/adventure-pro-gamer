package bsembilan.adventureprogamer.controller;

import bsembilan.adventureprogamer.core.charactercore.gamechar.Hero;
import bsembilan.adventureprogamer.service.characterservice.HeroServiceImpl;
import bsembilan.adventureprogamer.service.shopservice.ShopServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@WebMvcTest(controllers = ShopController.class)
class ShopControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ShopServiceImpl shopService;

    @MockBean
    private HeroServiceImpl heroService;

    private Hero testhero;

    @BeforeEach
    public void setUp() {
        testhero = Hero.getInstance();
    }

    @Test
    void whenShopURLIsAccessedItShouldContainCorrectShopModel() throws Exception {
        when(heroService.getHero()).thenReturn(testhero);
        mockMvc.perform(get("/shop"))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("items"))
                .andExpect(model().attributeExists("logs"))
                .andExpect(model().attributeExists("hero"))
                .andExpect(view().name("shop/shop"));
        verify(shopService, times(1)).getAllShopLogs();
        verify(shopService, times(1)).getAllItems();
        verify(heroService, times(1)).getHero();
    }

    @Test
    void whenBuyURLIsAccessedItShouldCallBuy() throws Exception {
        mockMvc.perform(post("/shop/buy")
                .param("itemType", "Advent Tusk"))
                .andExpect(handler().methodName("buyItem"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/shop"));
        verify(shopService, times(1)).buy("Advent Tusk");
    }
}
