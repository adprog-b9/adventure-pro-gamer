package bsembilan.adventureprogamer.controller;


import bsembilan.adventureprogamer.core.battleevent.adapter.BattleEventAdapter;
import bsembilan.adventureprogamer.core.battleevent.template.BattleEvent;
import bsembilan.adventureprogamer.core.battleevent.template.Goblin;
import bsembilan.adventureprogamer.core.battleevent.template.MainEnemy;
import bsembilan.adventureprogamer.core.charactercore.gamechar.Hero;
import bsembilan.adventureprogamer.core.eventcore.eventmain.dangerousmain.DangerousEvent;
import bsembilan.adventureprogamer.core.eventcore.eventmain.MainEvent;
import bsembilan.adventureprogamer.service.battleeventservice.BattleServiceImpl;
import bsembilan.adventureprogamer.service.characterservice.HeroServiceImpl;
import bsembilan.adventureprogamer.service.difficultyservice.CalculationServiceImpl;
import bsembilan.adventureprogamer.service.maineventservice.MainEventServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = EventController.class)
class EventControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private MainEventServiceImpl mainEventService;

    @MockBean
    private BattleServiceImpl battleService;

    @MockBean
    private CalculationServiceImpl calculationService;

    @MockBean
    private HeroServiceImpl heroService;

    private MainEvent battleEvent;
    private MainEvent mainEvent;
    private Hero testHero;

    @BeforeEach
    public void setUp() {
        mainEvent = new DangerousEvent("testmainevent");
        mainEvent.setActionMin("abc", "cde");
        mainEvent.setActionMed("abc", "fgh");
        mainEvent.setActionMax("abc", "ijk");
        mainEvent.setDescription("aaaa");

        BattleEvent goblin = new Goblin("Anak Goblin", 500, 60);
        battleEvent = new BattleEventAdapter(goblin);
        battleEvent.setDescription("Wahh, kamu diserang Anak Goblin.");

        testHero = Hero.getInstance();
        testHero.createChar("ddelvo", "Paladin", "Pacil");

    }

    @Test
    void whenRandomEventGeneratedItShouldContainEventDescriptionAndChoices() throws Exception {

        when(mainEventService.getRandomEvent()).thenReturn(mainEvent);
        when(heroService.getHero()).thenReturn(testHero);

        mvc.perform(get("/event/check"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("randomEvent"))
                .andExpect(model().attributeExists("randomEvent"))
                .andExpect(model().attributeExists("ActionMin"))
                .andExpect(model().attributeExists("ActionMed"))
                .andExpect(model().attributeExists("ActionMax"))
                .andExpect(view().name("event/event"));
        verify(mainEventService, times(1)).getRandomEvent();
    }

    @Test
    void whenBattleEventGeneratedItShouldServeBattleScreen() throws Exception {

        BattleEvent tempBattleEvent = battleEvent.getBattleEvent();

        BattleEvent dummyInstance = new MainEnemy(tempBattleEvent.getEnemyName(),
                tempBattleEvent.getEnemyHP(),
                tempBattleEvent.getEnemyAtt(),
                testHero,
                tempBattleEvent.getDbReward(),
                tempBattleEvent.getGameProgress()
        );

        when(mainEventService.getRandomEvent()).thenReturn(battleEvent);
        when(heroService.getHero()).thenReturn(testHero);
        when(battleService.createNewInstance(testHero, battleEvent.getBattleEvent())).thenReturn(dummyInstance);
        when(calculationService.getBattleIntervalFinal(testHero.getDifficulty())).thenReturn(2);

        mvc.perform(get("/event/check"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("randomEvent"))
                .andExpect(model().attributeExists("event"))
                .andExpect(view().name("event/battleevent"));
    }

    @Test
    void whenEndingAheadItShouldRedirectToDanger() throws Exception {
        BattleEvent dragon = new MainEnemy("Bahamut", 500, 60, testHero, 200, 100);
        when(heroService.getHero()).thenReturn(testHero);
        testHero.getCharStats().addExperience(100);

        when(battleService.createBossInstance(testHero)).thenReturn(dragon);

        mvc.perform(get("/event/check"))
                .andExpect(status().isFound())
                .andExpect(handler().methodName("randomEvent"));

    }

    @Test
    void whenRandomEventIsCalledButNoHeroItShouldRedirectToCharacterCreationScreen() throws Exception {
        var testHero2 = Hero.getInstance();
        testHero2.createChar(null, "Paladin", "Pacil");
        when(heroService.getHero()).thenReturn(testHero2);

        mvc.perform(get("/event/check"))
                .andExpect(status().isFound());
    }

    @Test
    void whenAddEventFormPageIsAccessedItShouldServeFormHTML() throws Exception {

        mvc.perform(get("/event/addEventForm"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("addEventForm"))
                .andExpect(view().name("event/addevent"));
    }

    @Test
    void whenEventCreationIsRequestedItShouldCreateTheRequestedEvent() throws Exception {

        when(mainEventService.createMainEvent("testmainevent", "danger", "aaaa")).thenReturn(mainEvent);
        when(mainEventService.setAction("testmainevent","abc","cde", 0)).thenReturn(null);
        when(mainEventService.setAction("testmainevent","abc","fgh", 1)).thenReturn(null);
        when(mainEventService.setAction("testmainevent","abc","ijk", 2)).thenReturn(null);


        mvc.perform(post("/event/addEvent")
                .param("eventName", "testmainevent")
                .param("eventDescription", "aaaa")
                .param("eventType", "danger")
                .param("mindes", "abc")
                .param("minres", "cde")
                .param("meddes","abc")
                .param("medres","fgh")
                .param("maxdes","abc")
                .param("maxres","ijk"))
                .andExpect(status().isFound())
                .andExpect(handler().methodName("addEvent"));


        verify(mainEventService, times(1)).createMainEvent("testmainevent", "danger", "aaaa");
        verify(mainEventService, times(1)).setAction("testmainevent","abc","cde", 0);
        verify(mainEventService, times(1)).setAction("testmainevent","abc","fgh", 1);
        verify(mainEventService, times(1)).setAction("testmainevent","abc","ijk", 2);
    }

    @Test
    void whenEventCreationIsRequestedButAlreadyExistedItShouldReturnException() throws Exception {

        when(mainEventService.createMainEvent("testmainevent", "danger", "aaaa")).thenReturn(null);
        when(mainEventService.setAction("testmainevent","abc","cde", 0)).thenReturn(null);
        when(mainEventService.setAction("testmainevent","abc","fgh", 1)).thenReturn(null);
        when(mainEventService.setAction("testmainevent","abc","ijk", 2)).thenReturn(null);

        mvc.perform(post("/event/addEvent")
                .param("eventName", "testmainevent")
                .param("eventDescription", "aaaa")
                .param("eventType", "danger")
                .param("mindes", "abc")
                .param("minres", "cde")
                .param("meddes","abc")
                .param("medres","fgh")
                .param("maxdes","abc")
                .param("maxres","ijk"))
                .andExpect(view().name("event/eventexisted"));

    }

    @Test
    void whenPlayerChoosesAttackItShouldAttack() throws Exception {
        when(battleService.Attack((float) 0.75, battleEvent)).thenReturn(425.0);
        mvc.perform(post("/event/attackEnemy").param("timing", "0.75")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("attackEnemy"));

    }

    @Test
    void whenPlayerChoosesDefendItShouldDefend() throws Exception {
        when(battleService.Defend((float) 0.75, battleEvent)).thenReturn(425.0);

        mvc.perform(post("/event/defendPlayer").param("timing", "0.75")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("defendPlayer"));

    }

    @Test
    void whenUrlIsLoseItShouldResetHero() throws Exception {
        mvc.perform(post("/event/lose"))
                .andExpect(status().isOk());

        verify(heroService, times(1)).resetCharInstance();
    }



    @Test
    void whenUrlIsWinItShouldAddDBAndProgress() throws Exception {
        mvc.perform(post("/event/win"))
                .andExpect(status().isOk());

        verify(heroService, times(1)).addDB(any(Integer.class));
        verify(heroService, times(1)).addExperience(any(Integer.class));
    }

    @Test
    void whenUrlIsWinAndItsTheEndItShouldResetHero() throws Exception {

        when(mainEventService.getIsTheEnd()).thenReturn(true);

        mvc.perform(post("/event/win"))
                .andExpect(status().isOk());

        verify(heroService, times(1)).resetCharInstance();
        verify(mainEventService, times(1)).setIsTheEnd(false);
    }

}
