package bsembilan.adventureprogamer.controller;

import bsembilan.adventureprogamer.core.charactercore.gamechar.Hero;
import bsembilan.adventureprogamer.service.characterservice.HeroService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = LandingController.class)
public class LandingControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private HeroService heroService;

    private Hero hero;

    @BeforeEach
    public void init(){
        hero = Hero.getInstance();

    }

    @Test
    void testWhenLandingURLIsAccessedShouldReturnTheCorrectTemplate() throws Exception {
        mockMvc.perform(get("/"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("landing"))
                .andExpect(view().name("landing/landingPage"));

    }

    @Test
    void testWhenConfirmationURLIsAccessedShouldReturnTheCorrectTemplateIfTheresAHero() throws Exception {
        hero.createChar("test","Paladin","Pacil");
        when(heroService.getHero()).thenReturn(hero);
        mockMvc.perform(get("/confirmation"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("confirmation"))
                .andExpect(view().name("landing/confirmation"));
    }

    @Test
    void testWhenConfirmationURLIsAccessedShouldRedirectCorrectlyIfTheresNoHero() throws Exception {
        hero.createChar(null,"Paladin","Pacil");
        when(heroService.getHero()).thenReturn(hero);
        mockMvc.perform(get("/confirmation"))
                .andExpect(status().is3xxRedirection())
                .andExpect(handler().methodName("confirmation"))
                .andExpect(redirectedUrl("/story"));
    }
}
