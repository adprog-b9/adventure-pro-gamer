package bsembilan.adventureprogamer.repository.shoprepository;

import bsembilan.adventureprogamer.core.shopcore.AdventTusk;
import bsembilan.adventureprogamer.core.shopcore.DefineFeeling;
import bsembilan.adventureprogamer.core.shopcore.Item;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.stereotype.Repository;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;

@Repository
class ShopRepositoryTest {
    private ShopRepository shopRepository;

    @Mock
    private Map<String, Item> items;

    @Mock
    private List<String> shopLogs;

    private Item adventTusk;

    @BeforeEach
    public void setUp() {
        shopRepository = new ShopRepositoryImpl();
        items = new HashMap<>();
        shopLogs = new ArrayList<>();
        adventTusk = new AdventTusk();
        items.put(adventTusk.getType(), adventTusk);
        shopLogs.add("You bought a special item");
    }

    @Test
    void whenShopRepoGetShopAllItShouldReturnShopLogs() {
        ReflectionTestUtils.setField(shopRepository, "shopLogs", shopLogs);
        List<String> acquiredLogs = shopRepository.getShopLog();

        assertThat(acquiredLogs).isEqualTo(shopLogs);
    }

    @Test
    void whenShopRepoGetItemsItShouldReturnItemsList() {
        ReflectionTestUtils.setField(shopRepository, "items", items);
        List<Item> acquiredItems = shopRepository.getItems();

        assertThat(acquiredItems).isEqualTo(new ArrayList(items.values()));
    }

    @Test
    void whenShopRepoAddShopLogItShouldSaveLog() {
        ReflectionTestUtils.setField(shopRepository, "shopLogs", shopLogs);
        String newLog = "You have bought a pog item";
        shopRepository.addShopLog(newLog);
        List<String> acquiredLogs = shopRepository.getShopLog();

        assertThat(acquiredLogs).isEqualTo(shopLogs);
        assertThat(shopLogs.get(1)).isEqualTo(newLog);
    }

    @Test
    void whenShopRepoAddItemShouldSaveItem() {
        ReflectionTestUtils.setField(shopRepository, "items", items);
        Item newItem = new DefineFeeling();
        shopRepository.addItem(newItem);
        Item acquiredItem = shopRepository.getItemByType(newItem.getType());

        assertThat(acquiredItem).isEqualTo(newItem);
    }

    @Test
    void whenShopRepoGetByTypeItShouldReturnItem() {
        ReflectionTestUtils.setField(shopRepository, "items", items);
        Item acquiredItem = shopRepository.getItemByType(adventTusk.getType());

        assertThat(acquiredItem).isEqualTo(adventTusk);
    }
}
