package bsembilan.adventureprogamer.repository.difficulty;

import bsembilan.adventureprogamer.core.difficulty.DaBabyStrategy;
import bsembilan.adventureprogamer.core.difficulty.DifficultyStrategy;

import bsembilan.adventureprogamer.repository.difficultyrepository.DifficultyRepository;
import bsembilan.adventureprogamer.repository.difficultyrepository.DifficultyRepositoryImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;


public class DifficultyRepositoryTest {
    private DifficultyStrategy dababy;

    DifficultyRepository difficultyRepository;

    @BeforeEach
    public void init(){
        difficultyRepository = new DifficultyRepositoryImpl();
        dababy = new DaBabyStrategy();
    }

    @Test
    public void testWhenFindAllIsCalledItShouldReturnAList(){
        difficultyRepository.save(dababy);
        ArrayList<DifficultyStrategy> list = new ArrayList<>();
        list.add(dababy);
        Iterable<DifficultyStrategy> list2 = new ArrayList<>(list);
        assertThat(list2).isEqualTo(difficultyRepository.findAll());
    }

    @Test
    public void testWhenFindByTypeIsCalledItShouldReturnADifficultyStrategy(){
        difficultyRepository.save(dababy);
        assertThat(dababy).isEqualTo(difficultyRepository.findByType("DaBaby"));
    }

    @Test
    public void testWhenSaveIsCalledItShouldReturnADifficultyStrategy(){
        assertThat(dababy).isEqualTo(difficultyRepository.save(dababy));
    }

    @Test
    public void testWhenSaveIsCalledIfThereIsExistingItShouldReturnADifficultyStrategy(){
        difficultyRepository.save(dababy);
        assertThat(dababy).isEqualTo(difficultyRepository.save(dababy));
    }



}
