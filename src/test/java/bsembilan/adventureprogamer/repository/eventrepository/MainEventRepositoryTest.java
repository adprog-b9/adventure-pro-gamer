package bsembilan.adventureprogamer.repository.eventrepository;
import bsembilan.adventureprogamer.core.eventcore.eventmain.dangerousmain.DangerousEvent;
import bsembilan.adventureprogamer.core.eventcore.eventmain.MainEvent;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class MainEventRepositoryTest {

    private MainEventRepository mainEventRepository;
    private DangerousEvent dangerousEvent;

    @BeforeEach
    public void setUp() {
        mainEventRepository = new MainEventRepositoryImpl();
        dangerousEvent = new DangerousEvent("Test Trap");
        mainEventRepository.add(dangerousEvent);
    }

    @Test
    void testGetEventList() {
        assertNotNull(mainEventRepository.getEventList());
        assertTrue(mainEventRepository.getEventList() instanceof ArrayList);
    }

    @Test
    void testSetEventList() {
        List<MainEvent> mainEventListTest = new ArrayList<>();
        mainEventListTest.add(new DangerousEvent("Test Trap 2"));
        mainEventListTest.add(dangerousEvent);
        mainEventRepository.setEventList(mainEventListTest);
        assertEquals(2, mainEventRepository.getEventList().size());
    }

    @Test
    void testAddEvent() {
        assertEquals("Test Trap", mainEventRepository.getEventList().get(0).getEventName());
    }

    @Test
    void testGetSpecificEvent() {
        assertNotNull(mainEventRepository.getEvent("Test Trap"));
        assertNull(mainEventRepository.getEvent("Something not Exist"));
    }

}
