package bsembilan.adventureprogamer.service;

import bsembilan.adventureprogamer.core.battleevent.adapter.BattleEventAdapter;
import bsembilan.adventureprogamer.core.battleevent.template.BattleEvent;
import bsembilan.adventureprogamer.core.battleevent.template.Goblin;
import bsembilan.adventureprogamer.core.eventcore.eventmain.MainEvent;
import bsembilan.adventureprogamer.repository.eventrepository.MainEventRepository;
import bsembilan.adventureprogamer.service.battleeventservice.BattleServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.InjectMocks;
import org.mockito.Mock;

public class BattleServiceTest {

    @Mock
    MainEventRepository mainEventRepository;

    @InjectMocks
    BattleServiceImpl battleService;

    private MainEvent battleEvent;

    @BeforeEach
    public void setUp() {
        battleService = new BattleServiceImpl();
        BattleEvent goblin = new Goblin("Anak Goblin", 50, 25);
        battleEvent = new BattleEventAdapter(goblin);
    }

}
