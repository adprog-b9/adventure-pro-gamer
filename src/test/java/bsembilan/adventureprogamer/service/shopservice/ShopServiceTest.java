package bsembilan.adventureprogamer.service.shopservice;

import bsembilan.adventureprogamer.core.charactercore.gamechar.Hero;
import bsembilan.adventureprogamer.core.shopcore.AdventTusk;
import bsembilan.adventureprogamer.core.shopcore.DefineFeeling;
import bsembilan.adventureprogamer.core.shopcore.Item;
import bsembilan.adventureprogamer.repository.shoprepository.ShopRepositoryImpl;
import bsembilan.adventureprogamer.service.characterservice.HeroServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ShopServiceTest {
    private Class<?> shopServiceClass;

    private Hero hero;

    @Spy
    private ShopRepositoryImpl shopRepository;

    @InjectMocks
    ShopService shopService = new ShopServiceImpl();

    @Mock
    private HeroServiceImpl heroService;

    @BeforeEach
    public void setUp() throws Exception {
        shopServiceClass = Class.forName(
                "bsembilan.adventureprogamer.service.shopservice.ShopServiceImpl");
        hero = Hero.getInstance();
    }

    @Test
    void testShopServiceHasGetAllShopLogsMethod() throws Exception {
        Method getAllShopLogs = shopServiceClass.getDeclaredMethod("getAllShopLogs");
        int methodModifiers = getAllShopLogs.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));

        ParameterizedType pt = (ParameterizedType) getAllShopLogs.getGenericReturnType();
        assertEquals(List.class, pt.getRawType());
        assertTrue(Arrays.asList(pt.getActualTypeArguments()).contains(String.class));
    }

    @Test
    void testShopServiceGetAllShopLogsImplementation() throws Exception {
        shopService.getAllShopLogs();
        verify(shopRepository, atLeastOnce()).getShopLog();
    }

    @Test
    void testShopServiceHasGetAllItemsMethod() throws Exception {
        Method getAllItems = shopServiceClass.getDeclaredMethod("getAllItems");
        int methodModifiers = getAllItems.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));

        ParameterizedType pt = (ParameterizedType) getAllItems.getGenericReturnType();
        assertEquals(List.class, pt.getRawType());
        assertTrue(Arrays.asList(pt.getActualTypeArguments()).contains(Item.class));
    }

    @Test
    void testShopServiceGetAllItemsReturnsCorrectItems() throws Exception {
        shopRepository.addItem(new AdventTusk());
        shopRepository.addItem(new DefineFeeling());

        assertEquals(2, shopService.getAllItems().size());
        verify(shopRepository, atLeastOnce()).getItems();
    }

    @Test
    void testShopServiceHasBuyMethod() throws Exception {
        Method buy = shopServiceClass.getDeclaredMethod("buy", String.class);
        int methodModifiers = buy.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    void testShopServiceBuyMethodCorrectlyImplemented() throws Exception {
        hero.getCharStats().addDB(200);
        hero.getCharStats().setMaxAttack(500);
        lenient().when(heroService.getHero()).thenReturn(hero);
        Item mockItem = new AdventTusk();
        shopRepository.addItem(mockItem);

        when(shopRepository.getItemByType(mockItem.getType())).thenReturn(mockItem);
        shopService.buy(mockItem.getType());
        verify(shopRepository, atLeastOnce()).getItemByType(any(String.class));
        verify(shopRepository, atLeastOnce()).addShopLog(any(String.class));

        assertThat(hero.getCharStats().getMaxAttack()).isEqualTo(520);
        assertThat(hero.getCharStats().getDB()).isEqualTo(100);
    }

    @Test
    void testShopServiceGetItemReturnsCorrectItem() throws Exception {
        Item mockItem = new AdventTusk();
        shopRepository.addItem(mockItem);

        when(shopRepository.getItemByType(mockItem.getType())).thenReturn(mockItem);
        Item item = shopService.getItem("Advent Tusk");
        assertThat(item).isEqualTo(mockItem);
    }
}
