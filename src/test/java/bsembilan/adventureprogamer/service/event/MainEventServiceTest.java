package bsembilan.adventureprogamer.service.event;

import bsembilan.adventureprogamer.core.eventcore.eventmain.dangerousmain.DangerousEvent;
import bsembilan.adventureprogamer.core.eventcore.eventmain.MainEvent;
import bsembilan.adventureprogamer.core.eventcore.eventmain.neutralmain.NeutralEvent;
import bsembilan.adventureprogamer.core.eventcore.eventmain.safemain.SafeEvent;
import bsembilan.adventureprogamer.repository.eventrepository.MainEventRepository;
import bsembilan.adventureprogamer.service.maineventservice.MainEventServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class MainEventServiceTest {

    @Mock
    MainEventRepository mainEventRepository;

    @InjectMocks
    MainEventServiceImpl mainEventService;

    private MainEvent randomeventDanger;
    private MainEvent randomeventNeutral;
    private MainEvent randomeventSafe;


    @BeforeEach
    public void setUp() {
        randomeventDanger = new DangerousEvent("EventRandomDanger");
        randomeventDanger.setDescription("abc");

        randomeventNeutral = new NeutralEvent("EventRandomNeutral");
        randomeventNeutral.setDescription("abc");

        randomeventSafe = new SafeEvent("EventRandomSafe");
        randomeventSafe.setDescription("abc");
        randomeventSafe.setActionMin("min", "mindes");
        randomeventSafe.setActionMed("med", "meddes");
        randomeventSafe.setActionMax("max", "maxdes");
    }

    @Test
    void testServiceGetRandomEvent() {

        List<MainEvent> eventList = new ArrayList<>();
        when(mainEventRepository.getEventList()).thenReturn(eventList);

        Answer answer = invocation -> {
            eventList.add(randomeventDanger);
            return randomeventDanger;
        };

        when(mainEventRepository.add(randomeventDanger)).thenAnswer(answer);
        mainEventRepository.add(randomeventDanger);


        assertNotNull(mainEventService.getRandomEvent());
        assertEquals("EventRandomDanger", mainEventService.getRandomEvent().getEventName());

        var onGoingEventNumber = mainEventService.getOnGoingEvent().size();
        mainEventService.clearOngoingEvent();
        assertNotEquals(onGoingEventNumber, mainEventService.getOnGoingEvent().size());
        mainEventService.addOngoingEvent(randomeventDanger);
        assertEquals(onGoingEventNumber, mainEventService.getOnGoingEvent().size());
    }

    @Test
    void testServiceCreateMainEventDanger() {

        lenient().when(mainEventRepository.add(any(DangerousEvent.class))).thenReturn(randomeventDanger);
        when(mainEventRepository.getEvent("EventRandomDanger")).thenReturn(randomeventDanger);

        mainEventService.createMainEvent("EventRandomDanger","Danger", "abc" );
        assertEquals("EventRandomDanger", mainEventRepository.getEvent("EventRandomDanger").getEventName());

    }

    @Test
    void testServiceCreateMainEventNeutral() {

        lenient().when(mainEventRepository.add(any(NeutralEvent.class))).thenReturn(randomeventNeutral);
        when(mainEventRepository.getEvent("EventRandomNeutral")).thenReturn(randomeventNeutral);

        mainEventService.createMainEvent("EventRandomNeutral","Neutral", "abc" );
        assertEquals("EventRandomNeutral", mainEventRepository.getEvent("EventRandomNeutral").getEventName());

    }

    @Test
    void testServiceCreateMainEventSafe() {

        lenient().when(mainEventRepository.add(any(SafeEvent.class))).thenReturn(randomeventSafe);
        when(mainEventRepository.getEvent("EventRandomSafe")).thenReturn(randomeventSafe);

        mainEventService.createMainEvent("EventRandomSafe","Safe", "abc");
        assertEquals("EventRandomSafe", mainEventRepository.getEvent("EventRandomSafe").getEventName());


    }

    @Test
    void testServiceSetAction() {
        when(mainEventRepository.getEvent("EventRandomNeutral")).thenReturn(randomeventNeutral);

        assertNull(mainEventService.setAction("EventRandomNeutral", "abc", "def", 0));
        assertNull(mainEventService.setAction("EventRandomNeutral", "abc", "def", 1));
        assertNull(mainEventService.setAction("EventRandomNeutral", "abc", "def", 2));
    }

    @Test
    void testServiceGetEvent() {
        when(mainEventRepository.getEvent("EventRandomNeutral")).thenReturn(randomeventNeutral);
        assertEquals("EventRandomNeutral", mainEventService.getEvent("EventRandomNeutral").getEventName());
    }

    @Test
    void testServiceDecodeEventName() {
        assertEquals("Event Random Neutral", mainEventService.decodeEventName("Event%20Random%20Neutral"));
    }

    @Test
    void testSetGetTheEnd() {
        assertFalse(mainEventService.getIsTheEnd());
        mainEventService.setIsTheEnd(true);
        assertTrue(mainEventService.getIsTheEnd());

    }

    @Test
    void testServiceChooseAction() {

        assertEquals("mindes", mainEventService.chooseAction(randomeventSafe, 0).getResultDescription());
        assertEquals("meddes", mainEventService.chooseAction(randomeventSafe, 1).getResultDescription());
        assertEquals("maxdes", mainEventService.chooseAction(randomeventSafe, 2).getResultDescription());
        assertNull(mainEventService.chooseAction(randomeventSafe, 3));
    }

    @Test
    void testServiceSetChoosen() {
        mainEventService.setChoosen(randomeventSafe, false);
        assertFalse(randomeventSafe.getHasChoosen());
    }

}
