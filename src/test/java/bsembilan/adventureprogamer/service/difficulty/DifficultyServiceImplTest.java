package bsembilan.adventureprogamer.service.difficulty;

import bsembilan.adventureprogamer.core.charactercore.gamechar.Hero;
import bsembilan.adventureprogamer.core.difficulty.DaBabyStrategy;
import bsembilan.adventureprogamer.core.difficulty.DifficultyStrategy;
import bsembilan.adventureprogamer.repository.difficultyrepository.DifficultyRepositoryImpl;
import bsembilan.adventureprogamer.service.characterservice.HeroServiceImpl;
import bsembilan.adventureprogamer.service.difficultyservice.DifficultyServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
public class DifficultyServiceImplTest {
    private Class<?> difficultyServiceClass;
    private Hero hero;

    @Spy
    private DifficultyRepositoryImpl difficultyRepository;

    @Mock
    private HeroServiceImpl heroService;
    
    @InjectMocks
    DifficultyServiceImpl difficultyService = new DifficultyServiceImpl(difficultyRepository);

    @BeforeEach
    public void init() throws ClassNotFoundException {
        difficultyServiceClass = Class.forName(
                "bsembilan.adventureprogamer.service.difficultyservice.DifficultyServiceImpl");
        hero = Hero.getInstance();
    }

    @Test
    public void testWhenFindAllIsCalledItShouldCallDifficultyStrategyRepositoryFindAll() {
        DifficultyStrategy difficulty = new DaBabyStrategy();
        difficultyRepository.save(difficulty);
        List<DifficultyStrategy> difficultyList = new ArrayList<>();
        difficultyList.add(difficulty);
        lenient().when(difficultyRepository.findAll()).thenReturn(difficultyList);
        Iterable<DifficultyStrategy> calledDifficultyStrategyList = difficultyService.findAll();
        verify(difficultyRepository, times(1)).findAll();
        assertThat(calledDifficultyStrategyList.iterator().next())
                .isEqualTo(difficultyList.get(0));
    }

    @Test
    public void testWhenFindByTypeIsCalledItShouldCallDifficultyStrategyRepositoryFindByType() {
        DifficultyStrategy difficulty = new DaBabyStrategy();
        difficultyRepository.save(difficulty);
        when(difficultyRepository.findByType(difficulty.getType())).thenReturn(difficulty);
        DifficultyStrategy calledDifficultyStrategy = difficultyService.findByType(difficulty.getType());

        verify(difficultyRepository, times(1))

                .findByType(difficulty.getType());
        assertThat(calledDifficultyStrategy).isEqualTo(difficulty);
    }

    @Test
    public void testWhenChangeStrategyIsCalledItShouldChangeDifficultyStrategy() {
        DifficultyStrategy difficultyStrategy = new DaBabyStrategy();

        lenient().when(heroService.getHero()).thenReturn(hero);

        difficultyService.changeStrategy(difficultyStrategy.getType());

        verify(heroService, times(1))
                .getHero();
        assertThat(heroService.getHero().getDifficulty()).isEqualTo(difficultyStrategy.getType());
    }
}