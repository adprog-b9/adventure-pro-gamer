package bsembilan.adventureprogamer.service.difficulty;

import bsembilan.adventureprogamer.core.difficulty.DaBabyStrategy;
import bsembilan.adventureprogamer.core.difficulty.DifficultyStrategy;
import bsembilan.adventureprogamer.core.difficulty.NormalStrategy;
import bsembilan.adventureprogamer.core.difficulty.PacilStrategy;
import bsembilan.adventureprogamer.repository.difficultyrepository.DifficultyRepository;
import bsembilan.adventureprogamer.service.difficultyservice.CalculationServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CalculationServiceImplTest {
    private DifficultyStrategy dababy;
    private DifficultyStrategy normal;
    private DifficultyStrategy pacil;

    @Mock
    private DifficultyRepository difficultyRepository;

    @InjectMocks
    CalculationServiceImpl calculationService;

    @BeforeEach
    public void init(){
        dababy = new DaBabyStrategy();
        normal = new NormalStrategy();
        pacil = new PacilStrategy();
        difficultyRepository.save(dababy);
        difficultyRepository.save(normal);
        difficultyRepository.save(pacil);
    }

    @Test
    public void testWhenGetActionFinalIsCalledItShouldReturnCalculationDaBaby() {
        lenient().when(difficultyRepository.findByType("DaBaby")).thenReturn(dababy);
        int calculation = calculationService.getActionFinal("DaBaby", 200);
        verify(difficultyRepository, times(1)).findByType("DaBaby");
        assertThat(calculation).isEqualTo(100);
    }

    @Test
    public void testWhenGetActionFinalIsCalledItShouldReturnCalculationNormal() {
        lenient().when(difficultyRepository.findByType("Normal")).thenReturn(normal);
        int calculation = calculationService.getActionFinal("Normal", 200);
        verify(difficultyRepository, times(1)).findByType("Normal");
        assertThat(calculation).isEqualTo(200);
    }

    @Test
    public void testWhenGetActionFinalIsCalledItShouldReturnCalculationPacil() {
        lenient().when(difficultyRepository.findByType("Pacil")).thenReturn(pacil);
        int calculation = calculationService.getActionFinal("Pacil", 200);
        verify(difficultyRepository, times(1)).findByType("Pacil");
        assertThat(calculation).isEqualTo(300);
    }

    @Test
    public void testWhenGetRewardFinalIsCalledItShouldReturnCalculationDaBaby() {
        lenient().when(difficultyRepository.findByType("DaBaby")).thenReturn(dababy);
        int test = 20;
        int calculation = calculationService.getRewardFinal("DaBaby", test);
        verify(difficultyRepository, times(1)).findByType("DaBaby");
        assertThat(calculation).isEqualTo(30);
    }

    @Test
    public void testWhenGetRewardFinalIsCalledItShouldReturnCalculationNormal() {
        lenient().when(difficultyRepository.findByType("Normal")).thenReturn(normal);
        int test = 20;
        int calculation = calculationService.getRewardFinal("Normal", test);
        verify(difficultyRepository, times(1)).findByType("Normal");
        assertThat(calculation).isEqualTo(20);
    }

    @Test
    public void testWhenGetRewardFinalIsCalledItShouldReturnCalculationPacil() {
        lenient().when(difficultyRepository.findByType("Pacil")).thenReturn(pacil);
        int test = 20;
        int calculation = calculationService.getRewardFinal("Pacil", test);
        verify(difficultyRepository, times(1)).findByType("Pacil");
        assertThat(calculation).isEqualTo(10);
    }

    @Test
    public void testWhenGetBattleIntervalFinalIsCalledItShouldReturnCalculationDaBaby() {
        lenient().when(difficultyRepository.findByType("DaBaby")).thenReturn(dababy);
        int calculation = calculationService.getBattleIntervalFinal("DaBaby");
        verify(difficultyRepository, times(1)).findByType("DaBaby");
        assertThat(calculation).isEqualTo(4);
    }
    @Test
    public void testWhenGetBattleIntervalFinalIsCalledItShouldReturnCalculationNormal() {
        lenient().when(difficultyRepository.findByType("Normal")).thenReturn(normal);
        int calculation = calculationService.getBattleIntervalFinal("Normal");
        verify(difficultyRepository, times(1)).findByType("Normal");
        assertThat(calculation).isEqualTo(3);
    }
    @Test
    public void testWhenGetBattleIntervalFinalIsCalledItShouldReturnCalculationPacil() {
        lenient().when(difficultyRepository.findByType("Pacil")).thenReturn(pacil);
        int calculation = calculationService.getBattleIntervalFinal("Pacil");
        verify(difficultyRepository, times(1)).findByType("Pacil");
        assertThat(calculation).isEqualTo(2);
    }
}