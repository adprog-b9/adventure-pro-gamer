package bsembilan.adventureprogamer.service.battleEvent;

import bsembilan.adventureprogamer.core.battleevent.adapter.BattleEventAdapter;
import bsembilan.adventureprogamer.core.battleevent.template.BattleEvent;
import bsembilan.adventureprogamer.core.battleevent.template.Goblin;
import bsembilan.adventureprogamer.core.eventcore.eventmain.MainEvent;
import bsembilan.adventureprogamer.repository.eventrepository.MainEventRepository;
import bsembilan.adventureprogamer.service.battleeventservice.BattleServiceImpl;
import bsembilan.adventureprogamer.service.characterservice.HeroServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(MockitoExtension.class)
class BattleEventImplTest {
    @Mock
    MainEventRepository mainEventRepository;

    @InjectMocks
    BattleServiceImpl battleService;

    @InjectMocks
    HeroServiceImpl heroService;

    private Goblin goblinEvent;
    private BattleEventAdapter battleEventAdapter;
    private MainEvent mainEvent;

    @BeforeEach
    void setUp() {
        goblinEvent = new Goblin("Goblin", 100, 100);
        battleEventAdapter = new BattleEventAdapter(goblinEvent);
        mainEvent = battleEventAdapter;
        mainEvent.setDescription("A starving dragon appears upon you! Careful!");
        mainEventRepository.add(mainEvent);

        heroService.createChar("alex", "Paladin", "DaBaby");
    }

    @Test
    void testMethodDefendAndAttackRunsFine() {

        // note that Paladin has 100 HP, 150 Attack, and 150 Defense
        assertEquals(100, battleService.Attack(0, mainEvent));
        assertEquals(-100, battleService.Defend(0, mainEvent));
    }

    @Test
    void testCreateNewInstance() {

        BattleEvent battleEvent = battleService.createNewInstance(heroService.getHero(), mainEvent.getBattleEvent());
        assertNotNull(battleEvent);
    }

    @Test
    void testGetDB() {

        assertEquals(0, battleService.getDb());
    }

    @Test
    void testGetGameProgress() {

        assertEquals(0, battleService.getGameProgress());
    }

    @Test
    void testCreateBossInstanceRunsPerfectly() {

        BattleEvent bossInstance = battleService.createBossInstance(heroService.getHero());
        assertNotNull(bossInstance);
    }
}
